package com.datalock.blitz.ui.registerlogin.login;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseFragment;

public class LoginFragment extends BaseFragment<LoginView, LoginPresenter> {
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    protected LoginPresenter initPresenter() {
        return new LoginPresenter();
    }
}
