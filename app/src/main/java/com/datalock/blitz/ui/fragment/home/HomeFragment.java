package com.datalock.blitz.ui.fragment.home;

import android.content.Intent;
import android.view.View;

import com.datalock.blitz.data.base.BaseFragment;
import com.datalock.blitz.ui.detail.DetailActivity;

import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.ui.fragment.profile.ProfileFragment;

public class HomeFragment extends BaseFragment<HomeView, HomePresenter> implements HomeView{
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected HomePresenter initPresenter() {
        return new HomePresenter();
    }

    @Override
    public void callFragmentProfile(int userId) {
        addFragment(R.id.fragment_container, new ProfileFragment(), String.valueOf(userId));
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(requireActivity(), DetailActivity.class));
    }
}
