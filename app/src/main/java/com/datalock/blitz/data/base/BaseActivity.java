package com.datalock.blitz.data.base;


import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.Toast;


import com.benkkstudio.bsmob.BSMob;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.utils.SharedPref;


@SuppressWarnings("unchecked")
public abstract class BaseActivity<V extends MvpView, P extends BasePresenter<V>> extends AppCompatActivity implements MvpView{
    protected P presenter;
    protected ViewDataBinding dataBinding;
    protected abstract int getLayoutId();
    protected abstract void onStarting();
    protected abstract void onDestroyed();
    private File mRoot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (SharedPref.getSharedPref(this).contains("THEME_MODE") && SharedPref.getSharedPref(this).readBoolean("THEME_MODE")) {
            setTheme(R.style.FeedActivityThemeDark);
        }
        super.onCreate(savedInstanceState);
        try {
            dataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        } catch (Exception e) {
            if (dataBinding == null) {
                setContentView(getLayoutId());
            }
        }
        BaseViewModel<V, P> baseViewModel = ViewModelProviders.of(this).get(BaseViewModel.class);
        boolean isPresenterCreated = false;
        if (baseViewModel.getPresenter() == null) {
            baseViewModel.setPresenter(initPresenter());
            isPresenterCreated = true;
        }
        presenter = baseViewModel.getPresenter();
        presenter.attachLifecycle(getLifecycle());
        presenter.attachView((V) this);
        if (isPresenterCreated) {
            presenter.onPresenterCreated();
        }
        onStarting();
    }

    protected static void runCurrentActivity(final Activity activity, final Class nextClass, final ActivityOptionsCompat options) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, nextClass);
                activity.startActivity(intent, options.toBundle());
            }
        }, 500);
    }

    protected static void runCurrentActivity(final Activity activity, final Class nextClass, final ActivityOptions options) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, nextClass);
                activity.startActivity(intent, options.toBundle());
            }
        }, 500);
    }

    protected static void runCurrentActivity(Activity activity, Class nextClass) {
        activity.startActivity(new Intent(activity, nextClass));
    }
    @Override
    protected void onDestroy() {
        onDestroyed();
        super.onDestroy();
        presenter.detachLifecycle(getLifecycle());
        presenter.detachView();
    }

    protected abstract P initPresenter();

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    protected BaseFragment mBaseCurrentFragment;

    protected void callFragment(int layoutId, BaseFragment baseFragment) {
        getSupportFragmentManager().beginTransaction().replace(layoutId, baseFragment, baseFragment.getClass().getName()).commit();
    }

    public void replaceFragment(int layoutId, BaseFragment fragment, String name) {
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(layoutId, fragment, name);
        fragmentTransaction.commit();
    }

    public void addFragment(int layoutId, BaseFragment fragment, String name) {
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); ++i) {
            getSupportFragmentManager().popBackStack();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.hide(getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount()));
        fragmentTransaction.add(layoutId, fragment, name);
        fragmentTransaction.addToBackStack(name);
        fragmentTransaction.commit();
    }

    public void saveVideoStatus(String path) {
        File file = Environment.getExternalStorageDirectory();
        String desFileName = file.getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/" + "Videos/";
        File newDir = new File(desFileName);
        if (!newDir.isDirectory() && !newDir.exists()) {
            boolean directoryCreated = newDir.mkdirs();
            if (directoryCreated) {
                createNewFile(desFileName, path);
            }
        } else {
            createNewFile(desFileName, path);
        }
        BSMob.getInterstitial().show();
    }

    public void saveImageStatus(String path) {
        File file = Environment.getExternalStorageDirectory();
        String desFileName = file.getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/" + "Images/";
        File newDir = new File(desFileName);
        if (!newDir.isDirectory() && !newDir.exists()) {
            boolean directoryCreated = newDir.mkdirs();
            if (directoryCreated) {
                createNewFile(desFileName, path);
            }
        } else {
            createNewFile(desFileName, path);
        }
        BSMob.getInterstitial().show();
    }
    public void shareVideo(File mPath) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("video/*");
        File media = new File(mPath.toString());
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(media));
        startActivity(Intent.createChooser(share, "Share to"));
    }

    private void createNewFile(String desFileName, String path) {
        String savedStoryPath = desFileName + getName(path);
        File SaveStoryFile = new File(savedStoryPath);
        if (!SaveStoryFile.isFile() && !SaveStoryFile.exists()) {
            try {
                boolean fileCreated = SaveStoryFile.createNewFile();
                if (fileCreated) {
                    copyStatusIntoFile(savedStoryPath, path);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            showToast("File Already Exists, Status Saved");
        }
        mRoot = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/" + getResources().getString(R.string.app_name));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            File f = new File(desFileName, getName(path));
            new SingleMediaScanner(getApplicationContext(), f);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + desFileName)));
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + desFileName)));
        }
    }
    public class SingleMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {

        private MediaScannerConnection mMs;
        private File mFile;

        private SingleMediaScanner(Context context, File f) {
            mFile = f;
            mMs = new MediaScannerConnection(context, this);
            mMs.connect();
        }

        @Override
        public void onMediaScannerConnected() {
            mMs.scanFile(mFile.getAbsolutePath(), null);
        }

        @Override
        public void onScanCompleted(String path, Uri uri) {
            mMs.disconnect();
        }
    }

    private void copyStatusIntoFile(String savedStoryPath, String path) {
        try {
            FileChannel inComingChannel = new FileInputStream(new File(path)).getChannel();
            FileChannel destinationChannel = new FileOutputStream(new File(savedStoryPath)).getChannel();

            long id = destinationChannel.transferFrom(inComingChannel, 0, inComingChannel.size());
            if (id > 0) {
                showToast("Status Saved");
                reScanSdCard();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void reScanSdCard() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" +
                    Environment.getExternalStorageDirectory()));
            sendBroadcast(mediaScannerIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" +
                    Environment.getExternalStorageDirectory())));
        }
    }

    public String getName(String path) {
        return path.substring(45, path.length());
    }
    private void showToast(String string){
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }
}