package com.datalock.blitz.ui.search;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface SearchView extends MvpView {
    void startActivity(View view, String tag);
    void callFragmentProfile(int userId);
}
