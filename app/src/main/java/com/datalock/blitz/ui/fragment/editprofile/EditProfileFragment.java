package com.datalock.blitz.ui.fragment.editprofile;

import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.base.BaseFragment;

public class EditProfileFragment extends BaseFragment<EditProfileView, EditProfilePresenter> {
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_profile;
    }


    @Override
    protected EditProfilePresenter initPresenter() {
        return new EditProfilePresenter();
    }
}
