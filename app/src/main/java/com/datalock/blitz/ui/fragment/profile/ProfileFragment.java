package com.datalock.blitz.ui.fragment.profile;

import android.content.Intent;
import android.view.View;

import com.datalock.blitz.data.base.BaseFragment;
import com.datalock.blitz.ui.detail.DetailActivity;
import com.datalock.blitz.ui.fragment.editprofile.EditProfileFragment;

import uc.benkkstudio.bsvideostatus.R;

public class ProfileFragment extends BaseFragment<ProfileView, ProfilePresenter> implements ProfileView{
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    protected ProfilePresenter initPresenter() {
        return new ProfilePresenter();
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(requireActivity(), DetailActivity.class));
    }

    @Override
    public void editProfile() {
        addFragment(R.id.fragment_container, new EditProfileFragment(), new EditProfileFragment().getTag());
    }
}
