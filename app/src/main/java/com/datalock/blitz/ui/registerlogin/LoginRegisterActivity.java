package com.datalock.blitz.ui.registerlogin;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseActivity;
import com.datalock.blitz.ui.registerlogin.login.LoginFragment;
import com.datalock.blitz.ui.registerlogin.register.RegisterFragment;

public class LoginRegisterActivity extends BaseActivity<LoginRegisterView, LoginRegisterPresenter> {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_login_register;
    }

    @Override
    protected void onStarting() {
        callFragment(R.id.fragment_container, new LoginFragment());
    }

    public void callRegisterFragment(){
        callFragment(R.id.fragment_container, new RegisterFragment());
    }

    public void callLoginFragment(){
        callFragment(R.id.fragment_container, new LoginFragment());
    }
    @Override
    protected void onDestroyed() {

    }

    @Override
    protected LoginRegisterPresenter initPresenter() {
        return new LoginRegisterPresenter();
    }
}
