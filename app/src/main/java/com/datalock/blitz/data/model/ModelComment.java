package com.datalock.blitz.data.model;

public class ModelComment {
    public int cmid;
    public int comment_user_id;
    public String comment_message;
    public String comment_time;
    public int comment_video_id;
    public ModelUsers modelUsers;

    public ModelComment(int cmid, int comment_user_id, String comment_message, String comment_time, int comment_video_id, ModelUsers modelUsers) {
        this.cmid = cmid;
        this.comment_user_id = comment_user_id;
        this.comment_message = comment_message;
        this.comment_time = comment_time;
        this.comment_video_id = comment_video_id;
        this.modelUsers = modelUsers;
    }
}
