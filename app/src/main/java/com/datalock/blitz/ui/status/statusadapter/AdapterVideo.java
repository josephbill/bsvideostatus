package com.datalock.blitz.ui.status.statusadapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.datalock.blitz.data.base.BaseActivity;
import com.datalock.blitz.ui.status.statusmodel.ModelVideo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

public class AdapterVideo extends RecyclerView.Adapter<AdapterVideo.ViewHolder> {
    private Activity activity;
    private ArrayList<ModelVideo> arrayList;

    public AdapterVideo(Activity activity, ArrayList<ModelVideo> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView status_image;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            status_image = itemView.findViewById(R.id.status_image);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(activity, R.layout.lsv_item_status, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelVideo modelVideo = arrayList.get(position);
        Glide.with(activity)
                .load(modelVideo.video_bitmap)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.status_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_status, null);
                ImageView status_image = dialogView.findViewById(R.id.status_image);
                Glide.with(activity)
                        .load(modelVideo.video_bitmap)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .into(status_image);
                FrameLayout download_frame = dialogView.findViewById(R.id.download_frame);
                download_frame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((BaseActivity) activity).saveVideoStatus(modelVideo.video_path);
                    }
                });
                FrameLayout share_frame = dialogView.findViewById(R.id.share_frame);
                share_frame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((BaseActivity) activity).shareVideo(new File(modelVideo.video_path));
                    }
                });
                dialog.setView(dialogView);
                dialog.setCancelable(true);
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
