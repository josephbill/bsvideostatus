package com.datalock.blitz.ui.upload;

import androidx.databinding.DataBindingUtil;
import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseActivity;

import uc.benkkstudio.bsvideostatus.databinding.ActivityUploadBinding;

import android.content.Intent;
import android.view.MenuItem;

import java.util.Objects;

public class UploadActivity extends BaseActivity<UploadView, UploadPresenter> {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_upload;
    }

    @Override
    protected void onStarting() {
        ActivityUploadBinding binding = DataBindingUtil.setContentView(this, getLayoutId());
        presenter.initView(this, binding);
        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected UploadPresenter initPresenter() {
        return new UploadPresenter();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
