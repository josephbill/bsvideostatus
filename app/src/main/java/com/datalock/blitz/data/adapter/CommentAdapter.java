package com.datalock.blitz.data.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datalock.blitz.data.model.ModelComment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.widgets.TimeAgos;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<ModelComment> arrayList;
    public CommentAdapter(Activity activity, ArrayList<ModelComment> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_comment_in, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ModelComment modelComment = arrayList.get(position);
        Glide.with(activity)
                .load(modelComment.modelUsers.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(R.drawable.profile_default)
                .error(R.drawable.profile_default)
                .into(holder.commentImage);
        holder.commentMessage.setText(modelComment.comment_message);
        holder.commentName.setText(modelComment.modelUsers.user_name);
        holder.commentTime.setText(TimeAgos.parse(modelComment.comment_time));
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView commentImage;
        TextView commentMessage, commentName, commentTime;
        ViewHolder(View view) {
            super(view);
            commentImage = view.findViewById(R.id.commentImage);
            commentMessage = view.findViewById(R.id.commentMessage);
            commentName = view.findViewById(R.id.commentName);
            commentTime = view.findViewById(R.id.commentTime);
        }
    }
}
