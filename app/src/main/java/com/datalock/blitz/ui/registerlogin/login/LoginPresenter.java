package com.datalock.blitz.ui.registerlogin.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.ui.main.MainActivity;
import com.datalock.blitz.ui.registerlogin.LoginRegisterActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.ToasterDialog;
import com.datalock.blitz.data.utils.Variable;

public class LoginPresenter extends BasePresenter<LoginView> {
    public static final String USER_ID = "USER_ID";
    public static final String LOGIN_OR_SKIP = "LOGIN_OR_SKIP";
    public static final String SKIP = "SKIP";
    public static final String NONE = "NONE";
    private static final String LOGIN = "LOGIN";
    private static final String SAVED_EMAIL = "SAVED_EMAIL";
    private static final String SAVED_PASSWORD = "SAVED_PASSWORD";
    private static final String TOGGLE_CHECKED = "TOGGLE_CHECKED";
    private Activity activity;
    private View rootView;
    private EditText edit_email, edit_password;
    private CheckBox save_username;
    private ProgressLoader progressLoader;

    protected void initView(Activity activity, View rootView) {
        this.activity = activity;
        this.rootView = rootView;
        progressLoader = new ProgressLoader(activity);
        edit_email = rootView.findViewById(R.id.edit_email);
        edit_password = rootView.findViewById(R.id.edit_password);
        save_username = rootView.findViewById(R.id.save_username);
        if (SharedPref.getSharedPref(activity).contains(SAVED_EMAIL)) {
            edit_email.setText(SharedPref.getSharedPref(activity).read(SAVED_EMAIL));
            edit_password.setText(SharedPref.getSharedPref(activity).read(SAVED_PASSWORD));
            if (SharedPref.getSharedPref(activity).readBoolean(TOGGLE_CHECKED)) {
                save_username.setChecked(true);
            }
        }
        initClick();
    }

    private void initClick() {
        rootView.findViewById(R.id.label_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });
        rootView.findViewById(R.id.button_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginRegisterActivity) activity).callRegisterFragment();
            }
        });
        rootView.findViewById(R.id.button_skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.getSharedPref(activity).write(LOGIN_OR_SKIP, SKIP);
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
            }
        });
        rootView.findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginNow();
            }
        });
    }

    private void forgotPassword() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dialog_forgot, null);
        alertDialog.setView(view);
        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        final EditText editText = view.findViewById(R.id.edit_email);
        view.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.button_forgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                progressLoader.show();
                JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
                jsObj.addProperty("method_name", "forgot_password");
                jsObj.addProperty("user_email", editText.getText().toString());
                new BSJson.Builder(activity)
                        .setServer(Settings.server_url)
                        .setObject(jsObj)
                        .setPurchaseCode(Settings.purchase_code)
                        .setListener(new BSJsonOnSuccessListener() {
                            @Override
                            public void onSuccess(int statusCode, byte[] responseBody) {
                                try {
                                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                                    JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                                    JSONObject objJson = jsonArray.getJSONObject(0);
                                    String msg = objJson.getString("msg");
                                    ToasterDialog.show(activity, msg);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                progressLoader.stopLoader();
                            }

                            @Override
                            public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                                progressLoader.stopLoader();
                            }
                        })
                        .load();
            }
        });
        dialog.show();

    }

    private void loginNow() {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "user_login");
        jsObj.addProperty("user_email", edit_email.getText().toString());
        jsObj.addProperty("user_password", edit_password.getText().toString());
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            String msg = objJson.getString("msg");
                            int success = objJson.getInt("success");
                            switch (success) {
                                case 1:
                                    SharedPref.getSharedPref(activity).write(USER_ID, objJson.getString("user_id"));
                                    SharedPref.getSharedPref(activity).write(LOGIN_OR_SKIP, LOGIN);
                                    if (save_username.isChecked()) {
                                        SharedPref.getSharedPref(activity).write(SAVED_EMAIL, edit_email.getText().toString());
                                        SharedPref.getSharedPref(activity).write(SAVED_PASSWORD, edit_password.getText().toString());
                                        SharedPref.getSharedPref(activity).write(TOGGLE_CHECKED, save_username.isChecked());
                                    } else {
                                        if (SharedPref.getSharedPref(activity).contains(SAVED_EMAIL)) {
                                            SharedPref.getSharedPref(activity).write(SAVED_EMAIL, "");
                                            SharedPref.getSharedPref(activity).write(SAVED_PASSWORD, "");
                                            SharedPref.getSharedPref(activity).write(TOGGLE_CHECKED, save_username.isChecked());
                                        }
                                    }
                                    Intent intent = new Intent(activity, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    activity.startActivity(intent);
                                    activity.finish();
                                    break;
                                case 0:
                                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }
}
