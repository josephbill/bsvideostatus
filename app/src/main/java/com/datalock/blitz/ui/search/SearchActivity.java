package com.datalock.blitz.ui.search;

import androidx.databinding.DataBindingUtil;
import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseActivityAnimation;
import uc.benkkstudio.bsvideostatus.databinding.ActivitySearchBinding;
import com.datalock.blitz.ui.detail.DetailActivity;
import com.datalock.blitz.ui.fragment.profile.ProfileFragment;

import android.content.Intent;
import android.transition.Explode;
import android.view.View;

public class SearchActivity extends BaseActivityAnimation<SearchView, SearchPresenter> implements SearchView{
    private ActivitySearchBinding binding;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onStarting() {
        getWindow().setEnterTransition(new Explode());
        getWindow().setExitTransition(new Explode());
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        presenter.initView(this, binding);

    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected SearchPresenter initPresenter() {
        return new SearchPresenter();
    }

    @Override
    public void callFragmentProfile(int userId) {
        addFragment(R.id.fragment_container, new ProfileFragment(), String.valueOf(userId));
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(this, DetailActivity.class));
    }

    @Override
    public void onBackPressed(){
        if(binding.searchView.isSearchOpen()){
            binding.searchView.closeSearch(true);
        } else {
            finish();
        }
    }
}
