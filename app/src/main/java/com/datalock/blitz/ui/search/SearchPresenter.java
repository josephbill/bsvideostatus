package com.datalock.blitz.ui.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.OvershootInterpolator;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.ui.detail.DetailActivity;
import com.ferfalk.simplesearchview.SimpleSearchView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import com.datalock.blitz.data.adapter.SearchAdapter;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.model.ModelUsers;
import com.datalock.blitz.data.utils.Logger;
import com.datalock.blitz.data.utils.Variable;
import uc.benkkstudio.bsvideostatus.databinding.ActivitySearchBinding;

public class SearchPresenter extends BasePresenter<SearchView> {
    private Activity activity;
    private ActivitySearchBinding binding;
    private ArrayList<ModelHome> arrayList;
    protected void initView(Activity activity, ActivitySearchBinding binding){
        this.activity = activity;
        this.binding = binding;
        arrayList = new ArrayList<>();
        initButton();
        loadData();
    }

    private void initButton(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.searchView.showSearch(true);
            }
        }, 100);
        binding.imageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchView.showSearch(true);
            }
        });
        binding.imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
    }

    private void loadData(){
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "search_video");
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int vid = objJson.getInt("vid");
                                int cat_id = objJson.getInt("cat_id");
                                String video_title = objJson.getString("video_title");
                                String video_time = objJson.getString("video_time");
                                String video_image = objJson.getString("video_image");
                                String video_url = objJson.getString("video_url");
                                String video_type = objJson.getString("video_type");
                                String video_view = objJson.getString("video_view");
                                String video_share = objJson.getString("video_share");

                                int user_id;
                                String user_name;
                                String user_image;
                                if(objJson.getInt("video_user_id") == 0){
                                    user_id = 0;
                                    user_name = Settings.admin_name;
                                    user_image = Variable.app_logo;
                                    Logger.log(Variable.app_logo);
                                } else {
                                    user_id = objJson.getInt("user_id");
                                    user_name = objJson.getString("user_name");
                                    user_image = objJson.getString("user_image");
                                }

                                String category_name = objJson.getString("category_name");

                                int total_comment = objJson.getInt("total_comment");
                                arrayList.add(new ModelHome(vid, cat_id, video_title, video_time, video_image, video_url, video_type, video_view, video_share, total_comment,
                                        new ModelUsers(user_id, user_name, user_image), new ModelCategories(category_name)));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        initData();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void initData(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        final SearchAdapter homeAdapter = new SearchAdapter(activity, arrayList, new RecyclerViewClickListener<ModelHome>() {
            @Override
            public void onItemClick(View view, ModelHome item, int i) {
                switch (i){
                    case 0:
                        activity.startActivity(new Intent(activity, DetailActivity.class));
                        break;
                    case 1:
                        Variable.user_id = String.valueOf(item.modelUsers.user_id);
                        getMvpView().callFragmentProfile(item.modelUsers.user_id);
                        break;
                }
            }
        });
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(homeAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(2000);
        if(Settings.enable_scroll_bouncing_effect){
            scaleInAnimationAdapter.setInterpolator(new BounceInterpolator());
        } else {
            scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator(0.5f));
        }
        binding.recyclerView.setAdapter(scaleInAnimationAdapter);
        binding.searchView.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                homeAdapter.search(newText);
                return false;
            }

            @Override
            public boolean onQueryTextCleared() {
                homeAdapter.search("");
                return false;
            }
        });
    }
}

