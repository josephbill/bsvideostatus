package com.datalock.blitz.ui.fragment.bycategory;

import android.content.Intent;
import android.view.View;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseFragment;
import com.datalock.blitz.ui.detail.DetailActivity;
import com.datalock.blitz.ui.fragment.profile.ProfileFragment;

public class VideoCatFragment extends BaseFragment<VideoCatView, VideoCatPresenter> implements VideoCatView {
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected VideoCatPresenter initPresenter() {
        return new VideoCatPresenter();
    }

    @Override
    public void callFragmentProfile(int userId) {
        addFragment(R.id.fragment_container, new ProfileFragment(), String.valueOf(userId));
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(requireActivity(), DetailActivity.class));
    }
}
