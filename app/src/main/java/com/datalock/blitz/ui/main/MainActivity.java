package com.datalock.blitz.ui.main;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.benkkstudio.bsmob.BSMob;
import com.benkkstudio.bsmob.Interface.InterstitialListener;
import com.datalock.blitz.ui.fragment.categories.CategoriesFragment;
import com.datalock.blitz.ui.fragment.editprofile.EditProfileFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.Objects;

import androidx.databinding.DataBindingUtil;
import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseActivity;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import uc.benkkstudio.bsvideostatus.databinding.ActivityMainBinding;
import com.datalock.blitz.ui.fragment.bookmark.BookmarkFragment;
import com.datalock.blitz.ui.fragment.home.HomeFragment;
import com.datalock.blitz.ui.fragment.profile.ProfileFragment;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.datalock.blitz.ui.status.StatusFragment;
import com.datalock.blitz.ui.upload.UploadActivity;

public class MainActivity extends BaseActivity<MainView, MainPresenter> implements MainView {
    private boolean doubleClickToExit = false;
    public ActivityMainBinding binding;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onStarting() {
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        presenter.initView(this, binding);
        callFragment(R.id.fragment_container, new HomeFragment());
        loadInterstitial();
    }

    private void loadInterstitial(){
        new BSMob.interstitial(this)
                .setAdRequest(new AdRequest.Builder().build())
                .setId(Variable.interstitial_id)
                .setListener(new InterstitialListener() {
                    @Override
                    public void onAdLoaded(InterstitialAd interstitialAd) {

                    }

                    @Override
                    public void onAdFailed(InterstitialAd interstitialAd) {
                        interstitialAd.loadAd(new AdRequest.Builder().build());
                    }

                    @Override
                    public void onAdClosed(InterstitialAd interstitialAd) {
                        interstitialAd.loadAd(new AdRequest.Builder().build());
                    }
                })
                .show();
    }


    @Override
    protected void onDestroyed() {

    }

    @Override
    public void startActivity(View view) {
    }

    @Override
    public void whatsappClick() {
        replaceFragment(R.id.fragment_container, new StatusFragment(), new HomeFragment().getTag());
    }

    @Override
    public void homeNavClick() {
        replaceFragment(R.id.fragment_container, new HomeFragment(), new HomeFragment().getTag());
    }

    @Override
    public void categoriesNavClick() {
        replaceFragment(R.id.fragment_container, new CategoriesFragment(), new CategoriesFragment().getTag());
    }

    @Override
    public void profileNavClick() {
        Variable.user_id = SharedPref.getSharedPref(this).read(LoginPresenter.USER_ID);
        replaceFragment(R.id.fragment_container, new ProfileFragment(), new ProfileFragment().getTag());
    }

    @Override
    public void bookmarkNavClick() {
        replaceFragment(R.id.fragment_container, new BookmarkFragment(), new BookmarkFragment().getTag());
    }

    public void reattachBookmark(){
        replaceFragment(R.id.fragment_container, new BookmarkFragment(), new BookmarkFragment().getTag());
    }
    @Override
    public void centreClick() {
        startActivity(new Intent(MainActivity.this, UploadActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (presenter.slidingRootNav.isMenuOpened()) {
            presenter.slidingRootNav.closeMenu();
        } else if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            if (doubleClickToExit) {
                finish();
            }
            doubleClickToExit = true;
            Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleClickToExit = false;
                }
            }, 1000);
        }
    }

    public void resetMenuButton(){
        binding.imageMenu.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu));
        binding.imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.slidingRootNav.openMenu(true);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            presenter.uploadProfile(BitmapFactory.decodeFile(Objects.requireNonNull(images).get(0).getPath()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onUpload() {
        addFragment(R.id.fragment_container, new EditProfileFragment(), new EditProfileFragment().getTag());
    }

    @Override
    protected MainPresenter initPresenter() {
        return new MainPresenter();
    }
}
