package com.datalock.blitz.ui.fragment.home;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.EndlessRecyclerViewScrollListener;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.model.ModelUsers;
import com.datalock.blitz.data.utils.Helpers;
import com.datalock.blitz.data.utils.Logger;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.Variable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.adapter.HomeAdapter;

class HomePresenter extends BasePresenter<HomeView> {
    private Activity activity;
    private View rootView;
    private ProgressLoader progressLoader;
    private int page_count = 1, total_post;
    private HomeAdapter homeAdapter;
    private ArrayList<ModelHome> arrayList;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        progressLoader = new ProgressLoader(activity);
        progressLoader.show();
        initData();
        if(Helpers.isConnected(activity)){
            loadData(page_count);
        } else {
            Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData(final int page){
        progressLoader.show();
        arrayList = new ArrayList<>();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "all_video");
        jsObj.addProperty("page", page);
        jsObj.addProperty("limit", Settings.video_loading_position);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        Logger.log(new String(responseBody));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject object_total_post = jsonArray.getJSONObject(0);
                            total_post = object_total_post.getInt("total_post");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                if(Variable.native_options && Variable.native_loaded){
                                    if (i == (Settings.video_loading_position / 2)) {
                                        arrayList.add(new ModelHome(HomeAdapter.VIEW_NATIVE));
                                    }
                                }
                                int vid = objJson.getInt("vid");
                                int cat_id = objJson.getInt("cat_id");
                                String video_title = objJson.getString("video_title");
                                String video_time = objJson.getString("video_time");
                                String video_image = objJson.getString("video_image");
                                String video_url = objJson.getString("video_url");
                                String video_type = objJson.getString("video_type");
                                String video_view = objJson.getString("video_view");
                                String video_share = objJson.getString("video_share");
                                int user_id;
                                String user_name;
                                String user_image;
                                if(objJson.getInt("video_user_id") == 0){
                                    user_id = 0;
                                    user_name = Settings.admin_name;
                                    user_image = Variable.app_logo;
                                    Logger.log(Variable.app_logo);
                                } else {
                                    user_id = objJson.getInt("user_id");
                                    user_name = objJson.getString("user_name");
                                    user_image = objJson.getString("user_image");
                                }
                                String category_name = objJson.getString("category_name");

                                int total_comment = objJson.getInt("total_comment");
                                arrayList.add(new ModelHome(vid, cat_id, video_title, video_time, video_image, video_url, video_type, video_view, video_share, total_comment,
                                        new ModelUsers(user_id, user_name, user_image), new ModelCategories(category_name), HomeAdapter.VIEW_ITEM));
                            }
                            homeAdapter.insertData(arrayList);
                            page_count++;
                            progressLoader.stopLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void initData(){
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        homeAdapter = new HomeAdapter(activity, new ArrayList<ModelHome>(), new RecyclerViewClickListener<ModelHome>() {
            @Override
            public void onItemClick(View view, ModelHome item, int i) {
                switch (i){
                    case 0:
                        getMvpView().startActivity(view, item.video_title);
                        break;
                    case 1:
                        Variable.user_id = String.valueOf(item.modelUsers.user_id);
                        getMvpView().callFragmentProfile(item.modelUsers.user_id);
                        break;
                }

            }
        });
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (homeAdapter.getItemCount() < total_post) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            homeAdapter.removeLoading();
                            loadData(page_count);
                        }
                    }, 1000);
                } else {
                    homeAdapter.removeLoading();
                    Toast.makeText(activity, "No more video !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(homeAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(1000);
        scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator(0.5f));
        recyclerView.setAdapter(scaleInAnimationAdapter);
        progressLoader.stopLoader();
    }
}
