package com.datalock.blitz.data.utils;

public class Events {
    public static class isPlaying {
        private boolean play;
        public isPlaying(boolean play) {
            this.play = play;
        }
        public boolean getPlay() {
            return play;
        }
    }

    public static class isFullscreen {
        private boolean full;

        public isFullscreen(boolean full) {
            this.full = full;
        }

        public boolean getFullscreen() {
            return full;
        }
    }

}
