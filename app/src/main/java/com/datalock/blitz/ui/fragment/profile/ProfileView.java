package com.datalock.blitz.ui.fragment.profile;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface ProfileView extends MvpView {
    void startActivity(View view, String tag);
    void editProfile();
}
