package com.datalock.blitz.ui.fragment.categories;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.adapter.CategoriesAdapter;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.EndlessRecyclerViewScrollListener;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.Variable;

public class CategoriesPresenter extends BasePresenter<CategoriesView> {
    private Activity activity;
    private View rootView;
    private ProgressLoader progressLoader;
    private int page_count = 1, total_post;
    private ArrayList<ModelCategories> arrayList;
    private CategoriesAdapter categoriesAdapter;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        initDataCategories();
        progressLoader = new ProgressLoader(activity);
        progressLoader.show();
        loadData(page_count);
    }

    private void loadData(final int page){
        arrayList = new ArrayList<>();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "all_category");
        jsObj.addProperty("page", page);
        jsObj.addProperty("limit", Settings.category_loading_position);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject object_total_post = jsonArray.getJSONObject(0);
                            total_post = object_total_post.getInt("total_post");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                if(Variable.native_options && Variable.native_loaded){
                                    if (i == (Settings.video_loading_position / 2)) {
                                        arrayList.add(new ModelCategories(CategoriesAdapter.VIEW_NATIVE));
                                    }
                                }
                                int cid = objJson.getInt("cid");
                                String categories_name = objJson.getString("category_name");
                                String categories_image = objJson.getString("category_image");
                                arrayList.add(new ModelCategories(cid, categories_name, categories_image, CategoriesAdapter.VIEW_ITEM));

                            }
                            categoriesAdapter.insertData(arrayList);
                            page_count++;
                            progressLoader.stopLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void initDataCategories(){
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        categoriesAdapter = new CategoriesAdapter(activity,  new RecyclerViewClickListener<ModelCategories>() {
            @Override
            public void onItemClick(View view, ModelCategories item, int i) {
                Variable.cat_id = item.cid;
                getMvpView().onClickListener(item.cid);
            }
        });
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (categoriesAdapter.arrayList.size() <= total_post) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            categoriesAdapter.removeLoading();
                            loadData(page_count);
                        }
                    }, 1000);
                } else {
                    categoriesAdapter.removeLoading();
                    Toast.makeText(activity, "No more video !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(categoriesAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(1000);
        scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator(0.5f));
        recyclerView.setAdapter(scaleInAnimationAdapter);
    }
}
