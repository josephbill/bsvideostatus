package com.datalock.blitz.ui.fragment.categories;

import com.datalock.blitz.ui.fragment.bycategory.VideoCatFragment;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseFragment;

public class CategoriesFragment extends BaseFragment<CategoriesView, CategoriesPresenter> implements CategoriesView{
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_categories;
    }

    @Override
    protected CategoriesPresenter initPresenter() {
        return new CategoriesPresenter();
    }

    @Override
    public void onClickListener(int cat_id) {
        addFragment(R.id.fragment_container, new VideoCatFragment(), "category_" + cat_id);
    }
}
