package com.datalock.blitz.ui.status;

import android.content.Context;

import com.datalock.blitz.data.base.BasePresenter;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.FragmentManager;

import uc.benkkstudio.bsvideostatus.databinding.FragmentStatusBinding;
import com.datalock.blitz.ui.status.statusadapter.PagerAdapter;

class StatusPresenter extends BasePresenter<StatusView> {
    private Context context;
    private FragmentStatusBinding binding;
    private FragmentManager fragmentManager;
    StatusPresenter(Context context, FragmentStatusBinding binding, FragmentManager fragmentManager) {
        this.context = context;
        this.binding = binding;
        this.fragmentManager = fragmentManager;
        setTabLayout();
    }

    private void setTabLayout(){
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Images"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Videos"));
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final PagerAdapter adapter = new PagerAdapter(fragmentManager, binding.tabLayout.getTabCount());
        binding.pager.setAdapter(adapter);
        binding.pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
        binding.tabLayout.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
