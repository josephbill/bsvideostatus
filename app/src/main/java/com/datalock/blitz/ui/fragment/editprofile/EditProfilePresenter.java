package com.datalock.blitz.ui.fragment.editprofile;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;

public class EditProfilePresenter extends BasePresenter<EditProfileView> {
    private Activity activity;
    private View rootView;
    private ProgressLoader progressLoader;
    private ImageView profileImage, change_image;
    private Button button_update;
    private String password;
    private EditText profile_name, profile_bio, profile_email, profile_old_password, profile_new_password, profile_confirm_password;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        progressLoader = new ProgressLoader(activity);
        initViewData();
        loadUserDetail();
    }

    private void initViewData(){
        button_update = rootView.findViewById(R.id.button_update);
        change_image = rootView.findViewById(R.id.change_image);
        profileImage = rootView.findViewById(R.id.profileImage);
        profile_name = rootView.findViewById(R.id.profile_name);
        profile_bio = rootView.findViewById(R.id.profile_bio);
        profile_email = rootView.findViewById(R.id.profile_email);
        profile_old_password = rootView.findViewById(R.id.profile_old_password);
        profile_new_password = rootView.findViewById(R.id.profile_new_password);
        profile_confirm_password = rootView.findViewById(R.id.profile_confirm_password);
        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ImagePicker.with(activity)
                            .setFolderMode(true)
                            .setFolderTitle("Album")
                            .setImageTitle(activity.getResources().getString(R.string.app_name))
                            .setStatusBarColor("#4EC7FC")
                            .setToolbarColor("#4EC7FC")
                            .setProgressBarColor("#4EC7FC")
                            .setMultipleMode(false)
                            .setMaxSize(1)
                            .setShowCamera(false)
                            .start();
                } catch (Exception e) {
                    Log.e("error", e.toString());
                }
            }
        });
        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(profile_new_password.getText().toString().length() != 0){
                    if(profile_old_password.getText().toString().equals(password)){
                        if(profile_new_password.getText().toString().equals(profile_confirm_password.getText().toString())){
                            if(profile_new_password.getText().toString().length() > 6){
                                editProfile(true);
                            } else {
                                Toast.makeText(activity, "Password must equal 6 character or more", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(activity, "Password don't match", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, "Old password don't match on database", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    editProfile(false);
                }
            }
        });
    }

    private void editProfile(boolean withPassword){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "edit_profile");
        jsObj.addProperty("profile_name", profile_name.getText().toString());
        jsObj.addProperty("profile_bio", profile_bio.getText().toString());
        jsObj.addProperty("profile_email", profile_email.getText().toString());
        jsObj.addProperty("user_id", Variable.user_upload_id);
        if(withPassword){
            jsObj.addProperty("profile_new_password", profile_new_password.getText().toString());
        } else {
            jsObj.addProperty("profile_new_password", "");
        }
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        Toast.makeText(activity, "Update successfully!", Toast.LENGTH_SHORT).show();
                        progressLoader.stopLoader();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void loadUserDetail(){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "user_detail");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            Variable.user_upload_id = objJson.getInt("user_id");
                            Glide.with(activity)
                                    .load(objJson.getString("user_image"))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()
                                    .placeholder(R.drawable.profile_default)
                                    .error(R.drawable.profile_default)
                                    .into(profileImage);
                            password = objJson.getString("user_password");
                            profile_name.setText(objJson.getString("user_name"));
                            profile_bio.setText(objJson.getString("user_bio"));
                            profile_email.setText(objJson.getString("user_email"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressLoader.stopLoader();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }
}
