package com.datalock.blitz.ui.fragment.home;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface HomeView extends MvpView {
    void startActivity(View view, String tag);
    void callFragmentProfile(int userId);
}
