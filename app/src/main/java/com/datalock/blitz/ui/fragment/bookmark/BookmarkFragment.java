package com.datalock.blitz.ui.fragment.bookmark;

import android.content.Intent;
import android.view.View;

import com.datalock.blitz.data.base.BaseFragment;
import com.datalock.blitz.ui.detail.DetailActivity;
import com.datalock.blitz.ui.fragment.profile.ProfileFragment;

import uc.benkkstudio.bsvideostatus.R;

public class BookmarkFragment extends BaseFragment<BookmarkView, BookmarkPresenter> implements BookmarkView{
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bookmark;
    }

    @Override
    protected BookmarkPresenter initPresenter() {
        return new BookmarkPresenter();
    }

    @Override
    public void callFragmentProfile(int userId) {
        addFragment(R.id.fragment_container, new ProfileFragment(), String.valueOf(userId));
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(requireActivity(), DetailActivity.class));
    }
}
