package com.datalock.blitz.data.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.utils.Logger;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.ToasterDialog;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.data.widgets.TimeAgos;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.ads.nativetemplates.BSNative;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    public ArrayList<ModelHome> arrayList;
    private RecyclerViewClickListener<ModelHome> recyclerViewClickListener;
    public static final int VIEW_ITEM = 0;
    private static final int VIEW_PROGRESS = 1;
    public static final int VIEW_NATIVE = 2;
    private ProgressLoader progressLoader;
    public HomeAdapter(Activity activity, ArrayList<ModelHome> arrayList, RecyclerViewClickListener<ModelHome> recyclerViewClickListener) {
        this.arrayList = arrayList;
        this.activity = activity;
        this.recyclerViewClickListener = recyclerViewClickListener;
        progressLoader = new ProgressLoader(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_video, parent, false);
            viewHolder = new HomeHolder(view);
        } else if (viewType == VIEW_NATIVE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_native_big, parent, false);
            viewHolder = new NativeHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_progressbar, parent, false);
            viewHolder = new ProgressHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderPrent, final int position) {
        switch (arrayList.get(position).view_type){
            case VIEW_ITEM:
                final HomeHolder holder = (HomeHolder) holderPrent;
                final ModelHome modelHome = arrayList.get(position);
                JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
                jsObj.addProperty("method_name", "check_bookmark");
                jsObj.addProperty("vid", modelHome.vid);
                jsObj.addProperty("bookmark_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
                new BSJson.Builder(activity)
                        .setServer(Settings.server_url)
                        .setObject(jsObj)
                        .setPurchaseCode(Settings.purchase_code)
                        .setListener(new BSJsonOnSuccessListener() {
                            @Override
                            public void onSuccess(int statusCode, byte[] responseBody) {
                                try {
                                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                                    JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                                    JSONObject objJson = jsonArray.getJSONObject(0);
                                    Logger.log(objJson.getInt("success"));
                                    if(objJson.getInt("success") == 1){
                                        holder.icon_bookmarks.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_filled));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                            }
                        })
                        .load();
                holder.video_title.setText(modelHome.video_title);
                holder.video_uploaded.setText(TimeAgos.parse(modelHome.video_time));
                holder.video_profile.setText(modelHome.modelUsers.user_name);
                holder.text_views_count.setText(String.format("%s Views", modelHome.video_view));
                holder.text_share_count.setText(String.format("%s Share", modelHome.video_share));
                holder.text_comment_count.setText(String.format("%s Comment", modelHome.total_comment));
                holder.icon_bookmarks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)) {
                            progressLoader.show();
                            JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
                            jsObj.addProperty("method_name", "check_bookmark");
                            jsObj.addProperty("vid", modelHome.vid);
                            jsObj.addProperty("bookmark_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
                            new BSJson.Builder(activity)
                                    .setServer(Settings.server_url)
                                    .setObject(jsObj)
                                    .setPurchaseCode(Settings.purchase_code)
                                    .setListener(new BSJsonOnSuccessListener() {
                                        @Override
                                        public void onSuccess(int statusCode, byte[] responseBody) {
                                            try {
                                                JSONObject jsonObject = new JSONObject(new String(responseBody));
                                                JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                                                JSONObject objJson = jsonArray.getJSONObject(0);
                                                if(objJson.getInt("success") == 0){
                                                    addBookmark(position, holder);
                                                } else {
                                                    deleteBookmark(position, holder);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            progressLoader.stopLoader();
                                        }
                                        @Override
                                        public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                                            progressLoader.stopLoader();
                                        }
                                    })
                                    .load();
                        } else {
                            ToasterDialog.showLogin(activity);
                        }
                    }
                });

                RequestOptions options = new RequestOptions().frame(1);
                Glide.with(activity).asBitmap()
                        .load(modelHome.video_url)
                        .apply(options)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(holder.image_video);
                Glide.with(activity)
                        .load(modelHome.modelUsers.user_image)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .placeholder(R.drawable.profile_default)
                        .error(R.drawable.profile_default)
                        .into(holder.image_profile);
                holder.image_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!modelHome.modelUsers.user_name.equals(Settings.admin_name)) {
                            recyclerViewClickListener.onItemClick(v, modelHome, 1);
                        }
                    }
                });
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            Variable.arrayListDetail = new ArrayList<>();
                            Variable.arrayListDetail.addAll(arrayList);
                            Variable.view_post_position = position;
                            recyclerViewClickListener.onItemClick(view, modelHome, 0);
                    }
                });
                break;
            case VIEW_NATIVE:
                final NativeHolder nativeHolder = (NativeHolder) holderPrent;
                nativeHolder.native_view.setNativeAd(Variable.unifiedNativeAd);
                break;
        }
    }

    private void addBookmark(int position, final HomeHolder holder){
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "add_bookmark");
        jsObj.addProperty("vid", arrayList.get(position).vid);
        jsObj.addProperty("bookmark_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        holder.icon_bookmarks.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark_filled));
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void deleteBookmark(int position, final HomeHolder holder){
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "delete_bookmark");
        jsObj.addProperty("vid", arrayList.get(position).vid);
        jsObj.addProperty("bookmark_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        holder.icon_bookmarks.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bookmark));
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return arrayList.get(position).view_type;
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView video_title, video_profile, video_uploaded, text_views_count, text_comment_count, text_share_count;
        ImageView image_video, image_profile, image_play, icon_bookmarks;
        HomeHolder(View itemView) {
            super(itemView);
            video_title = itemView.findViewById(R.id.video_title);
            video_profile = itemView.findViewById(R.id.video_profile);
            video_uploaded = itemView.findViewById(R.id.video_uploaded);
            image_video = itemView.findViewById(R.id.image_video);
            image_profile = itemView.findViewById(R.id.image_profile);
            image_play = itemView.findViewById(R.id.image_play);
            text_views_count = itemView.findViewById(R.id.text_views_count);
            text_comment_count = itemView.findViewById(R.id.text_comment_count);
            text_share_count = itemView.findViewById(R.id.text_share_count);
            icon_bookmarks = itemView.findViewById(R.id.icon_bookmarks);
        }
    }

    class ProgressHolder extends RecyclerView.ViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);
        }
    }

    class NativeHolder extends RecyclerView.ViewHolder {
        BSNative native_view;
        NativeHolder(View itemView) {
            super(itemView);
            native_view = itemView.findViewById(R.id.native_view);
        }
    }

    public void insertData(ArrayList<ModelHome> arrayList) {
        this.arrayList.addAll(arrayList);
        notifyItemRangeInserted(getItemCount(), arrayList.size());
        if(arrayList.size() >= Settings.video_loading_position){
            this.arrayList.add(new ModelHome(VIEW_PROGRESS));
            notifyItemRangeInserted(getItemCount(), 1);
        }
    }

    public void removeLoading() {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).view_type == VIEW_PROGRESS) {
                arrayList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }
}