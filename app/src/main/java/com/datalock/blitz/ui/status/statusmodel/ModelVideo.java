package com.datalock.blitz.ui.status.statusmodel;

import android.graphics.Bitmap;

public class ModelVideo {
    public String video_name;
    public String video_path;
    public Bitmap video_bitmap;

    public ModelVideo(String video_name, String video_path, Bitmap video_bitmap) {
        this.video_name = video_name;
        this.video_path = video_path;
        this.video_bitmap = video_bitmap;
    }
}
