package com.datalock.blitz.ui.main;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

interface MainView extends MvpView {
    void whatsappClick();
    void homeNavClick();
    void categoriesNavClick();
    void bookmarkNavClick();
    void profileNavClick();
    void startActivity(View view);
    void onUpload();
    void centreClick();
}
