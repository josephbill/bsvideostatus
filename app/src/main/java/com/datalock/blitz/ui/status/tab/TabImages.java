package com.datalock.blitz.ui.status.tab;

import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.utils.SdCardHelper;

import uc.benkkstudio.bsvideostatus.databinding.TabStatusBinding;
import com.datalock.blitz.ui.status.statusadapter.AdapterImage;
import com.datalock.blitz.ui.status.statusmodel.ModelImage;

public class TabImages extends Fragment {
    public ArrayList<ModelImage> arrayList;
    private TabStatusBinding binding;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.tab_status, container, false);
        binding = (TabStatusBinding) viewDataBinding;
        arrayList = new ArrayList<>();
        loadStatus();
        setRecyclerView();
        return viewDataBinding.getRoot();
    }

    private void loadStatus() {
        boolean sdCardPresent = SdCardHelper.isSdCardPresent();
        if (sdCardPresent) {
            File file = Environment.getExternalStorageDirectory();
            String whatsAppPath = file.getAbsolutePath() + "/WhatsApp/Media/.Statuses/";
            File statusFile = new File(whatsAppPath);
            if (statusFile.isDirectory()) {
                File[] files = statusFile.listFiles();
                if (files != null && files.length != 0) {
                    for (File value : files) {
                        if (value.getName().contains(".jpg") || value.getName().contains(".jpeg") || value.getName().contains(".png")) {
                            arrayList.add(new ModelImage(value.getName(), value.getAbsolutePath()));
                        }
                    }
                }
            }
        }
    }

    private void setRecyclerView(){
        binding.swipeRefresh.setVisibility(arrayList.size() == 0 ? View.GONE : View.VISIBLE);
        binding.layoutNoData.setVisibility(arrayList.size() == 0 ? View.VISIBLE : View.GONE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        binding.recyclerView.setLayoutManager(gridLayoutManager);
        binding.recyclerView.setNestedScrollingEnabled(true);
        ((SimpleItemAnimator) Objects.requireNonNull(binding.recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        binding.recyclerView.setAdapter(new AdapterImage(requireActivity(), arrayList));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}