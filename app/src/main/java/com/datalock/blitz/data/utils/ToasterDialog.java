package com.datalock.blitz.data.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.datalock.blitz.ui.registerlogin.LoginRegisterActivity;

import uc.benkkstudio.bsvideostatus.R;

public class ToasterDialog {
    public static void show(Activity activity, String message){
        CustomDialog customDialog = new CustomDialog(activity);
        customDialog.setTitle(activity.getString(R.string.app_name));
        customDialog.setDialogType(CustomDialog.ONE_BUTTON);
        customDialog.setMessage(message);
        customDialog.show();
    }

    public static void showLogin(final Activity activity){
        final CustomDialog customDialog = new CustomDialog(activity);
        customDialog.setMessage(activity.getString(R.string.please_login_first));
        customDialog.setTitle(activity.getString(R.string.app_name));
        customDialog.setPositveButton("LOGIN", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, LoginRegisterActivity.class));
                customDialog.dismiss();
            }
        });
        customDialog.setNegativeButton("LATER", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }
}
