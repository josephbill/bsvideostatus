package com.datalock.blitz.ui.registerlogin.register;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.ui.registerlogin.LoginRegisterActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.Variable;

public class RegisterPresenter extends BasePresenter<RegisterView> {
    private Activity activity;
    private View rootView;
    private EditText edit_name, edit_email, edit_password, edit_confirm_password;
    private ProgressLoader progressLoader;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        progressLoader = new ProgressLoader(activity);
        initClick();
    }

    private void initClick(){
        rootView.findViewById(R.id.button_sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginRegisterActivity)activity).callLoginFragment();
            }
        });
        rootView.findViewById(R.id.button_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
    }

    private void validateData(){
        edit_name = rootView.findViewById(R.id.edit_name);
        edit_email = rootView.findViewById(R.id.edit_email);
        edit_password = rootView.findViewById(R.id.edit_password);
        edit_confirm_password = rootView.findViewById(R.id.edit_confirm_password);
        if(edit_name.getText().toString().equals("") || edit_email.getText().toString().equals("") || edit_password.getText().toString().equals("") || edit_confirm_password.getText().toString().equals("")){
            Toast.makeText(activity, "Please fill all column", Toast.LENGTH_SHORT).show();
        } else if (!isValidEmail(edit_email.getText().toString())){
            Toast.makeText(activity, "Email not valid", Toast.LENGTH_SHORT).show();
        } else if (!edit_password.getText().toString().equals(edit_confirm_password.getText().toString())){
            Toast.makeText(activity, "Password don't match", Toast.LENGTH_SHORT).show();
        } else {
            registerNow();
        }
    }

    private boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void registerNow(){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "user_register");
        jsObj.addProperty("user_id", new Random().nextInt(999999999));
        jsObj.addProperty("user_name", edit_name.getText().toString());
        jsObj.addProperty("user_email", edit_email.getText().toString());
        jsObj.addProperty("user_password", edit_password.getText().toString());
        jsObj.addProperty("user_image", "");
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            String msg = objJson.getString("msg");
                            int success = objJson.getInt("success");
                            switch (success){
                                case 1:
                                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                                    ((LoginRegisterActivity)activity).callLoginFragment();
                                    break;
                                case 0:
                                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressLoader.stopLoader();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }
}
