package com.datalock.blitz.data.base;

import android.view.View;

/**
 * Created by W3E16 on 10/20/2017.
 */

public interface RecyclerViewClickListener<T>{
    void onItemClick(View view, T item, int i);
}
