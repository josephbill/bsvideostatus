package com.datalock.blitz.data.model;

public class ModelUsers {
    public int user_id;
    public String user_name;
    public String user_image;

    public ModelUsers(int user_id, String user_name, String user_image) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_image = user_image;
    }
}
