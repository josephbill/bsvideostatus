package com.datalock.blitz.ui.fragment.bookmark;

import android.app.Activity;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.adapter.BookmarkAdapter;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.model.ModelUsers;
import com.datalock.blitz.data.utils.Helpers;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import uc.benkkstudio.bsvideostatus.R;

public class BookmarkPresenter extends BasePresenter<BookmarkView> {
    private Activity activity;
    private View rootView;
    private ProgressLoader progressLoader;
    private LinearLayout layout_no_data;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        Variable.arrayListHome = new ArrayList<>();
        progressLoader = new ProgressLoader(activity);
        layout_no_data = rootView.findViewById(R.id.layout_no_data);
        if(Helpers.isConnected(activity)){
            loadData();
        } else {
            Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData(){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "all_bookmark");
        jsObj.addProperty("bookmark_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int vid = objJson.getInt("vid");
                                int cat_id = objJson.getInt("cat_id");
                                String video_title = objJson.getString("video_title");
                                String video_time = objJson.getString("video_time");
                                String video_image = objJson.getString("video_image");
                                String video_url = objJson.getString("video_url");
                                String video_type = objJson.getString("video_type");
                                String video_view = objJson.getString("video_view");
                                String video_share = objJson.getString("video_share");

                                int user_id;
                                String user_name;
                                String user_image;
                                if(objJson.getInt("video_user_id") == 0){
                                    user_id = 0;
                                    user_name = Settings.admin_name;
                                    user_image = Variable.app_logo;
                                } else {
                                    user_id = objJson.getInt("user_id");
                                    user_name = objJson.getString("user_name");
                                    user_image = objJson.getString("user_image");
                                }

                                String category_name = objJson.getString("category_name");

                                int total_comment = objJson.getInt("total_comment");
                                Variable.arrayListHome.add(new ModelHome(vid, cat_id, video_title, video_time, video_image, video_url, video_type, video_view, video_share, total_comment,
                                        new ModelUsers(user_id, user_name, user_image), new ModelCategories(category_name)));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        initData();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void initData(){
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        if(Variable.arrayListHome.size() == 0){
            layout_no_data.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        BookmarkAdapter homeAdapter = new BookmarkAdapter(activity, Variable.arrayListHome, new RecyclerViewClickListener<ModelHome>() {
            @Override
            public void onItemClick(View view, ModelHome item, int i) {
                switch (i){
                    case 0:
                        getMvpView().startActivity(view, item.video_title);
                        break;
                    case 1:
                        Variable.user_id = String.valueOf(item.modelUsers.user_id);
                        getMvpView().callFragmentProfile(item.modelUsers.user_id);
                        break;
                }

            }
        });
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(homeAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(1000);
        scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator(0.5f));
        recyclerView.setAdapter(scaleInAnimationAdapter);
        progressLoader.stopLoader();
    }
}