package com.datalock.blitz.ui.fragment.bycategory;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface VideoCatView extends MvpView {
    void startActivity(View view, String tag);
    void callFragmentProfile(int userId);
}
