package com.datalock.blitz.data.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.utils.Variable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.widgets.TimeAgos;

public class SearchAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private ArrayList<ModelHome> arrayList;
    private ArrayList<ModelHome> arrayListSearch;
    private RecyclerViewClickListener<ModelHome> recyclerViewClickListener;
    private int ITEMS_PER_AD = 4;
    public SearchAdapter(Activity activity, ArrayList<ModelHome> arrayList, RecyclerViewClickListener<ModelHome> recyclerViewClickListener) {
        this.arrayList = arrayList;
        arrayListSearch = new ArrayList<>();
        arrayListSearch.addAll(arrayList);
        this.arrayList.clear();
        this.activity = activity;
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_video, parent, false);
        return new HomeHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderPrent, final int position) {
        final HomeHolder holder = (HomeHolder) holderPrent;
        final ModelHome modelHome = arrayList.get(position);
        holder.video_title.setText(modelHome.video_title);
        holder.video_uploaded.setText(TimeAgos.parse(modelHome.video_time));
        holder.video_profile.setText(modelHome.modelUsers.user_name);
        holder.text_views_count.setText(String.format("%s Views", modelHome.video_view));
        holder.text_share_count.setText(String.format("%s Share", modelHome.video_share));
        holder.text_comment_count.setText(String.format("%s Comment", modelHome.total_comment));
        RequestOptions options = new RequestOptions().frame(1);
        Glide.with(activity).asBitmap()
                .load(modelHome.video_url)
                .apply(options)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.image_video);
        Glide.with(activity)
                .load(modelHome.modelUsers.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.image_profile);
        holder.image_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClickListener.onItemClick(v, modelHome, 1);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Variable.arrayListDetail = new ArrayList<>();
                Variable.arrayListDetail.addAll(arrayList);
                Variable.view_post_position = position;
                recyclerViewClickListener.onItemClick(view, modelHome, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView video_title, video_profile, video_uploaded, text_views_count, text_comment_count, text_share_count;
        ImageView image_video, image_profile, image_play;
        HomeHolder(View itemView) {
            super(itemView);
            video_title = itemView.findViewById(R.id.video_title);
            video_profile = itemView.findViewById(R.id.video_profile);
            video_uploaded = itemView.findViewById(R.id.video_uploaded);
            image_video = itemView.findViewById(R.id.image_video);
            image_profile = itemView.findViewById(R.id.image_profile);
            image_play = itemView.findViewById(R.id.image_play);
            text_views_count = itemView.findViewById(R.id.text_views_count);
            text_comment_count = itemView.findViewById(R.id.text_comment_count);
            text_share_count = itemView.findViewById(R.id.text_share_count);
        }
    }

    public void search(String charText) {
        arrayList.clear();
        if (charText.toLowerCase(Locale.getDefault()).length() == 0) {
            arrayList.clear();
        } else {
            for (ModelHome wp : arrayListSearch) {
                if (wp.video_title.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase(Locale.getDefault()))) {
                    arrayList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}