package com.datalock.blitz.ui.detail;

import androidx.databinding.DataBindingUtil;
import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseActivity;
import uc.benkkstudio.bsvideostatus.databinding.FragmentDetailBinding;

import android.content.Intent;
import android.transition.ChangeBounds;
import android.view.View;

public class DetailActivity extends BaseActivity<DetailView, DetailPresenter> implements DetailView {
    FragmentDetailBinding binding;
    @Override
    protected void onStarting() {
        ChangeBounds transition = new ChangeBounds();
        transition.setDuration(500);
        getWindow().setEnterTransition(transition);
        getWindow().setExitTransition(transition);
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        presenter.initView(this, binding);
    }

    @Override
    protected void onDestroyed() {
        presenter.onBackPressed();
    }

    @Override
    public void onBackPressed(){
        presenter.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.isPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail;
    }

    @Override
    public void startActivity(View view, String tag) {
        startActivity(new Intent(this, DetailActivity.class));
        finish();
    }

    @Override
    protected DetailPresenter initPresenter() {
        return new DetailPresenter();
    }

}
