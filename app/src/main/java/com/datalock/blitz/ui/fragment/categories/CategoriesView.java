package com.datalock.blitz.ui.fragment.categories;

import com.datalock.blitz.data.base.MvpView;

public interface CategoriesView extends MvpView {
    void onClickListener(int cat_id);
}
