package com.datalock.blitz.ui.fragment.bookmark;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface BookmarkView extends MvpView {
    void startActivity(View view, String tag);
    void callFragmentProfile(int userId);
}
