package com.datalock.blitz.data.model;

public class ModelCategories {
    public int cid;
    public String categories_name;
    public String categories_image;
    public int view_type;
    public ModelCategories(String categories_name) {
        this.categories_name = categories_name;
    }

    public ModelCategories(int cid, String categories_name, String categories_image, int view_type) {
        this.cid = cid;
        this.categories_name = categories_name;
        this.categories_image = categories_image;
        this.view_type = view_type;
    }

    public ModelCategories(int cid, String categories_name, String categories_image) {
        this.cid = cid;
        this.categories_name = categories_name;
        this.categories_image = categories_image;
    }

    public ModelCategories(int view_type) {
        this.view_type = view_type;
    }
}
