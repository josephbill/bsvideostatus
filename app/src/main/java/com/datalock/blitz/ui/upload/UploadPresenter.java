package com.datalock.blitz.ui.upload;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.utils.Logger;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import uc.benkkstudio.bsvideostatus.databinding.ActivityUploadBinding;

import static android.app.Activity.RESULT_OK;

public class UploadPresenter extends BasePresenter<UploadView> {
    private Activity activity;
    private ActivityUploadBinding binding;
    private int REQUEST_GALLERY_PICKER = 100;
    private int REQUEST_CODE_CHOOSE = 0;
    private String video_image;
    private String video_path;
    private ArrayList<ModelCategories> arrayList;
    private int cat_id;
    private ProgressLoader progressLoader;
    private boolean isImageUploaded = false, isVideoUploaded  = false;
    protected void initView(Activity activity, ActivityUploadBinding binding){
        this.activity = activity;
        this.binding = binding;
        progressLoader = new ProgressLoader(activity);
        loadCategory();
        initButton();
    }

    private void initButton(){
        binding.buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(!isVideoUploaded){
                    Toast.makeText(activity, "Please upload video !", Toast.LENGTH_LONG).show();
                } else if(binding.videoTitle.getText().toString().length() == 0){
                    Toast.makeText(activity, "Video title can't be null !", Toast.LENGTH_LONG).show();
                } else {
                    uploadVideo();
                }
            }
        });
        binding.selectVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_upload = new Intent();
                intent_upload.setType("video/mp4");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                activity.startActivityForResult(intent_upload, REQUEST_CODE_CHOOSE);
            }
        });

    }
    private void uploadVideo(){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "upload_video");
        jsObj.addProperty("cat_id", cat_id);
        jsObj.addProperty("video_title", binding.videoTitle.getText().toString());
        jsObj.addProperty("video_url", video_path.substring(video_path.lastIndexOf("/")));
        jsObj.addProperty("video_user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        Toast.makeText(activity, "Video uploaded successfully", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void loadCategory(){
        arrayList = new ArrayList<>();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "upload_category");
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        Logger.log(new String(responseBody));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int cid = objJson.getInt("cid");
                                String categories_name = objJson.getString("category_name");
                                String categories_image = objJson.getString("category_image");
                                arrayList.add(new ModelCategories(cid, categories_name, categories_image));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        initCategory();
                    }
                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void initCategory(){
        ArrayList<String> categoryList = new ArrayList<>();
        for(int i = 0; i < arrayList.size(); i++){
            categoryList.add(arrayList.get(i).categories_name);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.spinner_item, categoryList);
        adapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
        binding.categorySpinner.setAdapter(adapter);
        binding.categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cat_id = arrayList.get(position).cid;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            String file_path = getPath(activity, data.getData());
            try {
                assert file_path != null;
                File file = new File(file_path);
                int file_size = (int) file.length() / (1024 * 1024);
                String file_name = file.getName();
                if (file_name.contains(".mp4")) {
                    video_path = file_path;
                    if (file_size <= Settings.video_upload_size) {
                        if (!(getDurationInt(file_path) <= Settings.video_upload_duration)) {
                            Toast.makeText(activity, activity.getString(R.string.duration_to_long), Toast.LENGTH_LONG).show();
                        } else {
                            binding.videoResult.setText(getPath(activity, data.getData()));
                            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(file_path, MediaStore.Images.Thumbnails.MINI_KIND);
                            if (thumb != null) {
                                downloadImage(thumb);
                            }
                            try {
                                progressLoader.show();
                                AsyncHttpClient client = new AsyncHttpClient();
                                RequestParams params = new RequestParams();
                                params.put("file", new File(file_path));
                                client.post(Settings.server_url.replace("api.php", "upload_video.php"), params, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        progressLoader.stopLoader();
                                        isVideoUploaded = true;
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        progressLoader.stopLoader();
                                    }
                                });
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.file_size_to_big), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.invalid_file_type), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Toast.makeText(activity, activity.getString(R.string.upload_error), Toast.LENGTH_LONG).show();
            }

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                video_image = result.getUri().getPath();
                try {
                    progressLoader.show();
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("file", new File(video_image));
                    client.post(Settings.server_url.replace("api.php", "upload_image.php"), params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            progressLoader.stopLoader();
                            isImageUploaded = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            progressLoader.stopLoader();
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void downloadImage(Bitmap bitmap) {
        String iconsStoragePath = Objects.requireNonNull(activity.getExternalCacheDir()).getAbsolutePath();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "image_upload" + n + ".jpg";
        File file = new File(iconsStoragePath, fname);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private String getPath(final Context context, final Uri uri) {
        if (DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            else if (isDownloadsDocument(uri)) {

                try {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(context, contentUri, null, null);
                } catch (Exception e) {
                    Log.d("error_data", e.toString());
                }

            }
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        final String column = "_data";
        final String[] projection = {
                column
        };
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private long getDurationInt(String filePath) {
        MediaMetadataRetriever metaRetriever_int = new MediaMetadataRetriever();
        metaRetriever_int.setDataSource(filePath);
        String songDuration = metaRetriever_int.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long duration = Long.parseLong(songDuration);
        long time = (int) duration / 1000;
        metaRetriever_int.release();
        return time;
    }


}
