package com.datalock.blitz.data.utils;

import java.security.SecureRandom;
import java.util.ArrayList;


public class RandomData {
    private static SecureRandom random = new SecureRandom();
    private static final ArrayList<String> arrayListImage = new ArrayList<String>() {
        {
            add("https://i.ytimg.com/vi/ktIgy-yPCkg/maxresdefault.jpg");
            add("https://i.ytimg.com/vi/5ZYn4fr5DfQ/maxresdefault.jpg");
            add("https://i.ytimg.com/vi/DgoUuw_uP8E/maxresdefault.jpg");
            add("https://i.ytimg.com/vi/c2C3F0PPkYg/maxresdefault.jpg");
            add("https://i.ytimg.com/vi/VM94TFMfsPw/maxresdefault.jpg");
            add("https://i.ytimg.com/vi/lQyez2ieZBg/maxresdefault.jpg");
        }
    };

    public static String randomImage() {
        return arrayListImage.get(random.nextInt(arrayListImage.size()));
    }

}
