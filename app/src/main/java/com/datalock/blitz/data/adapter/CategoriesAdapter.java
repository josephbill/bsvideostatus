package com.datalock.blitz.data.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datalock.blitz.Settings;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.utils.Variable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.ads.nativetemplates.BSNative;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private RecyclerViewClickListener<ModelCategories> recyclerViewClickListener;
    public ArrayList<ModelCategories> arrayList;
    public static final int VIEW_ITEM = 0;
    private static final int VIEW_PROGRESS = 1;
    public static final int VIEW_NATIVE = 2;
    public CategoriesAdapter(Activity activity, RecyclerViewClickListener<ModelCategories> recyclerViewClickListener) {
        this.activity = activity;
        this.arrayList = new ArrayList<>();
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_categories, parent, false);
            viewHolder = new HomeHolder(view);
        } else if (viewType == VIEW_NATIVE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_native_small, parent, false);
            viewHolder = new NativeHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_progressbar, parent, false);
            viewHolder = new ProgressHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderPrent, final int position) {

        switch (arrayList.get(position).view_type){
            case VIEW_ITEM:
                final HomeHolder holder = (HomeHolder) holderPrent;
                final ModelCategories modelCategories = arrayList.get(position);
                holder.text_categories.setText(modelCategories.categories_name);
                Glide.with(activity)
                        .load(modelCategories.categories_image)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .centerCrop()
                        .into(holder.image_categories);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        recyclerViewClickListener.onItemClick(v, modelCategories, position);
                    }
                });
                break;
            case VIEW_NATIVE:
                final NativeHolder nativeHolder = (NativeHolder) holderPrent;
                nativeHolder.native_view.setNativeAd(Variable.unifiedNativeAd);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return arrayList.get(position).view_type;
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView text_categories;
        ImageView image_categories;
        HomeHolder(View itemView) {
            super(itemView);
            image_categories = itemView.findViewById(R.id.image_categories);
            text_categories = itemView.findViewById(R.id.text_categories);
        }
    }

    class ProgressHolder extends RecyclerView.ViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);
        }
    }

    class NativeHolder extends RecyclerView.ViewHolder {
        BSNative native_view;
        NativeHolder(View itemView) {
            super(itemView);
            native_view = itemView.findViewById(R.id.native_view);
        }
    }

    public void insertData(ArrayList<ModelCategories> arrayList) {
        int positionStart = getItemCount();
        int itemCount = arrayList.size();
        this.arrayList.addAll(arrayList);
        notifyItemRangeInserted(positionStart, itemCount);
        if(arrayList.size() >= Settings.video_loading_position){
            this.arrayList.add(new ModelCategories(VIEW_PROGRESS));
            notifyItemInserted(arrayList.size() - 1);
        }
    }


    public void removeLoading() {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).view_type == VIEW_PROGRESS) {
                arrayList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }
}