package com.datalock.blitz.ui.registerlogin.register;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseFragment;

public class RegisterFragment extends BaseFragment<RegisterView, RegisterPresenter> {
    @Override
    protected void onStarting() {
        presenter.initView(requireActivity(), getRootView());
    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register;
    }

    @Override
    protected RegisterPresenter initPresenter() {
        return new RegisterPresenter();
    }
}
