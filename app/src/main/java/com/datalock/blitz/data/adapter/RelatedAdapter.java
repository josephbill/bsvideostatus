package com.datalock.blitz.data.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.utils.Variable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.widgets.TimeAgos;

public class RelatedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private ArrayList<ModelHome> arrayList;
    private RecyclerViewClickListener<ModelHome> recyclerViewClickListener;
    private int ITEMS_PER_AD = 4;

    public RelatedAdapter(Activity activity, ArrayList<ModelHome> arrayList, RecyclerViewClickListener<ModelHome> recyclerViewClickListener) {
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(arrayList);
        this.activity = activity;
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.lsv_item_related, parent, false);
        return new HomeHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderPrent, final int position) {
        final HomeHolder holder = (HomeHolder) holderPrent;
        final ModelHome modelHome = arrayList.get(position);
        holder.video_title.setText(modelHome.video_title);
        holder.video_uploaded.setText(TimeAgos.parse(modelHome.video_time));
        holder.video_profile.setText(modelHome.modelUsers.user_name);
        RequestOptions options = new RequestOptions().frame(1);
        Glide.with(activity).asBitmap()
                .load(modelHome.video_url)
                .apply(options)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.image_video);
        Glide.with(activity)
                .load(modelHome.modelUsers.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.image_profile);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Variable.arrayListDetail = new ArrayList<>();
                Variable.arrayListDetail.addAll(arrayList);
                Variable.view_post_position = position;
                recyclerViewClickListener.onItemClick(view, modelHome, 0);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView video_title, video_profile, video_uploaded;
        ImageView image_video, image_profile, image_play;
        HomeHolder(View itemView) {
            super(itemView);
            video_title = itemView.findViewById(R.id.video_title);
            video_profile = itemView.findViewById(R.id.video_profile);
            video_uploaded = itemView.findViewById(R.id.video_uploaded);
            image_video = itemView.findViewById(R.id.image_video);
            image_profile = itemView.findViewById(R.id.image_profile);
            image_play = itemView.findViewById(R.id.image_play);
        }
    }
}