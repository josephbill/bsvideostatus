package com.datalock.blitz.ui.status.tab;

import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.utils.SdCardHelper;
import uc.benkkstudio.bsvideostatus.databinding.TabStatusBinding;
import com.datalock.blitz.ui.status.statusadapter.AdapterVideo;
import com.datalock.blitz.ui.status.statusmodel.ModelVideo;

public class TabVideos extends Fragment {
    public ArrayList<ModelVideo> arrayList;
    public TabStatusBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.tab_status, container, false);
        binding = (TabStatusBinding) viewDataBinding;
        arrayList = new ArrayList<>();
        loadStatus();
        setRecyclerView();
        return viewDataBinding.getRoot();
    }

    private void loadStatus() {
        boolean sdCardPresent = SdCardHelper.isSdCardPresent();
        if (sdCardPresent) {
            File file = Environment.getExternalStorageDirectory();
            String whatsAppPath = file.getAbsolutePath() + "/WhatsApp/Media/.Statuses/";
            File statusFile = new File(whatsAppPath);
            if (statusFile.isDirectory()) {
                File[] files = statusFile.listFiles();
                if (files != null && files.length != 0) {
                    for (File value : files) {
                        if (value.getName().contains(".mp4") || value.getName().contains(".flv") || value.getName().contains(".mkv")) {
                            arrayList.add(new ModelVideo(value.getName(), value.getAbsolutePath(), ThumbnailUtils.createVideoThumbnail(value.getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND)));
                        }
                    }
                }
            }
        }
    }

    private void setRecyclerView() {
        binding.swipeRefresh.setVisibility(arrayList.size() == 0 ? View.GONE : View.VISIBLE);
        binding.layoutNoData.setVisibility(arrayList.size() == 0 ? View.VISIBLE : View.GONE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        binding.recyclerView.setLayoutManager(gridLayoutManager);
        binding.recyclerView.setNestedScrollingEnabled(true);
        ((SimpleItemAnimator) Objects.requireNonNull(binding.recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        binding.recyclerView.setAdapter(new AdapterVideo(requireActivity(), arrayList));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}