package com.datalock.blitz.ui.status.statusmodel;

public class ModelImage {
    public String image_name;
    public String image_path;

    public ModelImage(String image_name, String image_path) {
        this.image_name = image_name;
        this.image_path = image_path;
    }
}
