package com.datalock.blitz.data.utils;

import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelHome;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.io.Serializable;
import java.util.ArrayList;

public class Variable implements Serializable {
    public static final String TAG_ROOT = "BENKKSTUDIO";
    public static ArrayList<ModelHome> arrayListHome;
    public static ArrayList<ModelHome> arrayListProfile;
    public static ArrayList<ModelHome> arrayListDetail;
    public static ArrayList<ModelCategories> arrayListCategory;
    public static String user_id;
    public static int user_upload_id;
    public static int cat_id;
    public static int view_post_position;

    public static String app_email;
    public static String app_logo;
    public static boolean banner_options;
    public static String banner_id;
    public static boolean interstitial_options;
    public static String interstitial_id;
    public static int interstitial_click;
    public static boolean native_options;
    public static String native_id;
    public static String privacy_police;

    public static boolean native_loaded;
    public static UnifiedNativeAd unifiedNativeAd;
    public static int ads_count = 0;
}
