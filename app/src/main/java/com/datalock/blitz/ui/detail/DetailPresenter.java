package com.datalock.blitz.ui.detail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.benkkstudio.bsmob.BSMob;
import com.benkkstudio.bsmob.Interface.BannerListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.codekidlabs.storagechooser.StorageChooser;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Objects;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.adapter.CommentAdapter;
import com.datalock.blitz.data.adapter.RelatedAdapter;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelComment;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.model.ModelUsers;
import com.datalock.blitz.data.utils.CustomDialog;
import com.datalock.blitz.data.utils.Helpers;
import com.datalock.blitz.data.utils.Logger;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.ToasterDialog;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.data.widgets.TimeAgos;
import uc.benkkstudio.bsvideostatus.databinding.FragmentDetailBinding;

import static android.content.Context.DOWNLOAD_SERVICE;

public class DetailPresenter extends BasePresenter<DetailView> {
    public static final String VIDEO_POS = "VIDEO_POS";
    private Activity activity;
    private SimpleExoPlayer player;
    private FragmentDetailBinding binding;
    private Boolean mExoPlayerFullscreen = false;
    private TextView text_not_available;
    private Boolean collapseView = false;
    private MediaSource mediaSource;
    private ModelHome modelHome;
    private ProgressLoader progressLoader;
    private ArrayList<ModelHome> arrayListRelated = new ArrayList<>();
    private ArrayList<ModelComment> arrayListComment = new ArrayList<>();
    private String DOWNLOAD_FOLDER = "DOWNLOAD_FOLDER";
    private String SAVED_DOWNLOAD_FOLDER;
    private CustomDialog customDialog;
    public long downloadID;
    protected void initView(Activity activity, final FragmentDetailBinding binding) {
        this.activity = activity;
        this.binding = binding;
        progressLoader = new ProgressLoader(activity);
        modelHome = Variable.arrayListDetail.get(Variable.view_post_position);
        progressLoader.show();
        customDialog = new CustomDialog(activity);
        Variable.ads_count++;
        if(Variable.ads_count == Variable.interstitial_click){
            BSMob.getInterstitial().show();
            Variable.ads_count = 0;
        }
        if (Helpers.isConnected(activity)) {
            initViewData();
            initShareButton();
            initBannerAds();
            initExoPlayer();
            loadRelated();
            initComment();
            sendView();
            checkFollower();
        } else {
            Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("DefaultLocale")
    private void initViewData() {

        binding.videoTitle.setText(modelHome.video_title);
        binding.videoProfile.setText(modelHome.modelUsers.user_name);
        binding.videoUploaded.setText(TimeAgos.parse(modelHome.video_time));
        RequestOptions options = new RequestOptions().frame(1000);
        Glide.with(activity).asBitmap()
                .load(modelHome.video_url)
                .apply(options)
                .into(binding.imagePreview);
        Glide.with(activity)
                .load(modelHome.modelUsers.user_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(R.drawable.profile_default)
                .error(R.drawable.profile_default)
                .into(binding.imageProfile);
        binding.imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.setPlayWhenReady(true);
                binding.framePreview.setVisibility(View.GONE);
                binding.frameVideo.setVisibility(View.VISIBLE);
            }
        });
        if (SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID).contains(Integer.toString(modelHome.modelUsers.user_id))) {
            binding.buttonFollow.setVisibility(View.GONE);
        }

        binding.sendCommentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)) {
                    dialogComment();
                } else {
                    ToasterDialog.showLogin(activity);
                }

            }
        });
    }

    private void dialogComment() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_comment);
        Objects.requireNonNull(dialog.getWindow()).setLayout(ViewPager.LayoutParams.FILL_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        final EditText text_comment = dialog.findViewById(R.id.send_comment_text);
        dialog.findViewById(R.id.image_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text_comment.getText().toString().length() != 0) {
                    sendComment(text_comment.getText().toString());
                    text_comment.setText("");
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void initShareButton() {
        binding.shareFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOn(v.getTag().toString());
            }
        });
        binding.shareInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOn(v.getTag().toString());
            }
        });
        binding.shareTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOn(v.getTag().toString());
            }
        });
        binding.shareWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOn(v.getTag().toString());
            }
        });
        binding.shareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareOn(v.getTag().toString());
            }
        });
        binding.downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), activity.getString(R.string.download_folder));
                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs();
                }
                if (Helpers.isConnected(activity)) {
                    if(SharedPref.getSharedPref(activity).contains(DOWNLOAD_FOLDER)){
                        SAVED_DOWNLOAD_FOLDER = SharedPref.getSharedPref(activity).read(DOWNLOAD_FOLDER);
                        downloadFile();
                    } else {
                        chooseDownloadDirectory();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void downloadFile() {
        if (!new File(Environment.getExternalStorageDirectory().getPath() + File.separator + SAVED_DOWNLOAD_FOLDER + File.separator + modelHome.video_title + ".mp4").exists()) {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(modelHome.video_url));
            DownloadManager downloadManager = (DownloadManager) activity.getSystemService(DOWNLOAD_SERVICE);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setAllowedOverRoaming(false);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setTitle("Downloading " + modelHome.video_title + ".mp4");
            request.setDescription("Downloading " + modelHome.video_title + ".mp4");
            request.setVisibleInDownloadsUi(true);
            request.setDestinationInExternalPublicDir(SAVED_DOWNLOAD_FOLDER, modelHome.video_title + ".mp4");
            assert downloadManager != null;
            downloadID = downloadManager.enqueue(request);
            ToasterDialog.show(activity, activity.getString(R.string.download_started));
            BSMob.getInterstitial().show();
        } else {
            ToasterDialog.show(activity, activity.getString(R.string.file_exist));
        }

    }

    private void chooseDownloadDirectory() {
        StorageChooser chooser = new StorageChooser.Builder()
                .withActivity(activity)
                .allowCustomPath(true)
                .setType(StorageChooser.DIRECTORY_CHOOSER)
                .withFragmentManager(activity.getFragmentManager())
                .build();
        chooser.show();
        chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
            @Override
            public void onSelect(String path) {
                SAVED_DOWNLOAD_FOLDER = path.replace("/storage/emulated/0/", "");
                saveDialog(path.replace("/storage/emulated/0/", ""));
            }
        });
    }

    private void saveDialog(final String path) {
        customDialog.setMessage("Do you want save this folder ?");
        customDialog.setTitle("Save Folder");
        customDialog.setPositveButton("SAVE", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.getSharedPref(activity).write(DOWNLOAD_FOLDER, path);
                downloadFile();
                customDialog.dismiss();
            }
        });
        customDialog.setNegativeButton("NO, THANKS", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                downloadFile();
            }
        });
        customDialog.show();
    }

    private void shareOn(String packagename) {
        player.setPlayWhenReady(false);
        if (!packagename.equals("share_more")) {
            if (Helpers.isAppInstalled(activity, packagename)) {
                if (Helpers.isConnected(activity)) {
                    new ShareVideo().execute(modelHome.video_url, "" + modelHome.vid, packagename);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, packagename + " not installed!", Toast.LENGTH_SHORT).show();
            }
        } else {
            new ShareVideo().execute(modelHome.video_url, "" + modelHome.vid, packagename);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class ShareVideo extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String iconsStoragePath;
        private File sdIconStorageDir;
        private String type;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage("Please wait ..");
            progressDialog.setCancelable(false);
            progressDialog.setMax(100);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (sdIconStorageDir != null) {
                        sdIconStorageDir.delete();
                    }
                    dialog.dismiss();
                    cancel(true);
                }
            });
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            int count;
            try {
                URL url = new URL(params[0]);
                String id = params[1];
                type = params[2];
                iconsStoragePath = activity.getExternalCacheDir().getAbsolutePath();
                String filePath = "file" + id + ".mp4";

                sdIconStorageDir = new File(iconsStoragePath, filePath);
                if (sdIconStorageDir.exists()) {
                    Log.d("File_name", sdIconStorageDir.toString());
                } else {
                    URLConnection conection = url.openConnection();
                    conection.setRequestProperty("Accept-Encoding", "identity");
                    conection.connect();
                    int lenghtOfFile = conection.getContentLength();
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    OutputStream output = new FileOutputStream(sdIconStorageDir);
                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        progressDialog.setProgress((int) (total * 100 / lenghtOfFile));
                        Log.d("progressDialog", String.valueOf((int) (total * 100 / lenghtOfFile)));
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            sendShare();
            switch (type) {
                case "com.whatsapp":
                    Uri uri = Uri.parse(sdIconStorageDir.toString());
                    Intent videoshare = new Intent(Intent.ACTION_SEND);
                    videoshare.setType("*/*");
                    videoshare.setPackage("com.whatsapp");
                    videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    videoshare.putExtra(Intent.EXTRA_STREAM, uri);
                    activity.startActivity(videoshare);
                    break;
                case "com.facebook.katana":
                    Intent share_fb = new Intent(Intent.ACTION_SEND);
                    share_fb.setType("video/*");
                    share_fb.setPackage("com.facebook.katana");
                    File media_fb = new File(sdIconStorageDir.toString());
                    Uri uri_fb = Uri.fromFile(media_fb);
                    share_fb.putExtra(Intent.EXTRA_STREAM, uri_fb);
                    activity.startActivity(Intent.createChooser(share_fb, "Share to"));
                    break;
                case "com.instagram.android":
                    try {
                        Intent intent = new Intent("com.instagram.share.ADD_TO_STORY");
                        intent.setDataAndType(Uri.fromFile(sdIconStorageDir), "video/*");
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra("content_url", "");
                        if (activity.getPackageManager().resolveActivity(intent, 0) != null) {
                            activity.startActivityForResult(intent, 0);
                        }
                    } catch (Exception e) {
                        Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "com.twitter.android":
                    Intent share_tw = new Intent(Intent.ACTION_SEND);
                    share_tw.setType("video/*");
                    share_tw.setPackage("com.twitter.android");
                    File media_tw = new File(sdIconStorageDir.toString());
                    Uri uri_tw = Uri.fromFile(media_tw);
                    share_tw.putExtra(Intent.EXTRA_STREAM, uri_tw);
                    activity.startActivity(Intent.createChooser(share_tw, "Share to"));
                    break;
                case "share_more":
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("video/*");
                    File media = new File(sdIconStorageDir.toString());
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(media));
                    activity.startActivity(Intent.createChooser(share, "Share to"));
                    break;
                default:
                    break;
            }
        }

    }

    private void initBannerAds() {
        new BSMob.banner(activity)
                .setAdRequest(new AdRequest.Builder().build())
                .setId(Variable.banner_id)
                .setLayout(binding.adsView)
                .setListener(new BannerListener() {
                    @Override
                    public void onAdFailedToLoad(int error) {
                        binding.adsView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAdLoaded() {
                        binding.adsView.setVisibility(View.VISIBLE);
                    }
                })
                .setSize(BSMob.adaptiveSize(activity))
                .show();
    }

    private void sendView() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "send_view");
        jsObj.addProperty("vid", modelHome.vid);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .load();
    }

    private void sendShare() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "send_share");
        jsObj.addProperty("vid", modelHome.vid);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .load();
    }

    private void checkFollower() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "check_follower");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", modelHome.modelUsers.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            if (objJson.getInt("success") == 1) {
                                binding.buttonFollow.setText("UNFOLLOW");
                                binding.buttonFollow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendUnFollow();
                                    }
                                });
                            } else {
                                binding.buttonFollow.setText("FOLLOW");
                                binding.buttonFollow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)) {
                                            sendFollow();
                                        } else {
                                            ToasterDialog.showLogin(activity);
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {

                    }
                })
                .load();
    }

    private void sendUnFollow() {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "un_follow");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", modelHome.modelUsers.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        binding.buttonFollow.setText("FOLLOW");
                        binding.buttonFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)) {
                                    sendFollow();
                                } else {
                                    ToasterDialog.showLogin(activity);
                                }
                            }
                        });
                        progressLoader.stopLoader();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void sendFollow() {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "follow");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", modelHome.modelUsers.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        binding.buttonFollow.setText("UNFOLLOW");
                        binding.buttonFollow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sendUnFollow();
                            }
                        });
                        progressLoader.stopLoader();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void sendComment(String message) {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "send_comment");
        jsObj.addProperty("vid", modelHome.vid);
        jsObj.addProperty("comment_user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("comment_message", message);
        jsObj.addProperty("comment_video_id", modelHome.vid);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        progressLoader.show();
                        arrayListComment.clear();
                        loadComment();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void loadRelated() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "related_video");
        jsObj.addProperty("cat_id", modelHome.cat_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        Logger.log(new String(responseBody));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int vid = objJson.getInt("vid");
                                int cat_id = objJson.getInt("cat_id");
                                String video_title = objJson.getString("video_title");
                                String video_time = objJson.getString("video_time");
                                String video_image = objJson.getString("video_image");
                                String video_url = objJson.getString("video_url");
                                String video_type = objJson.getString("video_type");
                                String video_view = objJson.getString("video_view");
                                String video_share = objJson.getString("video_share");

                                int user_id;
                                String user_name;
                                String user_image;
                                if(objJson.getInt("video_user_id") == 0){
                                    user_id = 0;
                                    user_name = Settings.admin_name;
                                    user_image = Variable.app_logo;
                                    Logger.log(Variable.app_logo);
                                } else {
                                    user_id = objJson.getInt("user_id");
                                    user_name = objJson.getString("user_name");
                                    user_image = objJson.getString("user_image");
                                }

                                String category_name = objJson.getString("category_name");

                                int total_comment = objJson.getInt("total_comment");
                                arrayListRelated.add(new ModelHome(vid, cat_id, video_title, video_time, video_image, video_url, video_type, video_view, video_share, total_comment,
                                        new ModelUsers(user_id, user_name, user_image), new ModelCategories(category_name)));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < arrayListRelated.size(); i++) {
                            if (arrayListRelated.get(i).vid == modelHome.vid) {
                                arrayListRelated.remove(i);
                            }
                        }
                        initRelated();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void loadComment() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "detail_comment");
        jsObj.addProperty("vid", modelHome.vid);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int cmid = objJson.getInt("cmid");
                                int comment_user_id = objJson.getInt("comment_user_id");
                                String comment_message = objJson.getString("comment_message");
                                String comment_time = objJson.getString("comment_time");
                                int comment_video_id = objJson.getInt("comment_video_id");


                                int user_id = objJson.getInt("user_id");
                                String user_name = objJson.getString("user_name");
                                String user_image = objJson.getString("user_image");

                                arrayListComment.add(new ModelComment(cmid, comment_user_id, comment_message, comment_time, comment_video_id, new ModelUsers(user_id, user_name, user_image)));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        initComment();
                        binding.textTotalComment.setText(String.format("%d Comment's", arrayListComment.size()));
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void initRelated() {
        RelatedAdapter relatedAdapter = new RelatedAdapter(activity, arrayListRelated, new RecyclerViewClickListener<ModelHome>() {
            @Override
            public void onItemClick(View view, ModelHome item, int i) {
                getMvpView().startActivity(view, "related_" + item.video_title);
            }
        });
        AlphaInAnimationAdapter scaleInAnimationAdapter = new AlphaInAnimationAdapter(relatedAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(1000);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        binding.recyclerViewRelated.setLayoutManager(linearLayoutManager);
        binding.recyclerViewRelated.setAdapter(scaleInAnimationAdapter);
        loadComment();
    }

    private void initComment() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.recyclerViewComment.setLayoutManager(linearLayoutManager);
        binding.recyclerViewComment.setAdapter(new CommentAdapter(activity, arrayListComment));
        progressLoader.stopLoader();
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initExoPlayer() {
        TrackSelector trackSelector = new DefaultTrackSelector();
        LoadControl loadControl = new DefaultLoadControl();
        player = ExoPlayerFactory.newSimpleInstance(activity, trackSelector, loadControl);
        binding.videoPlayer.setPlayer(player);
        mediaSource = mediaSource(Uri.parse(modelHome.video_url));
        player.prepare(mediaSource, true, false);
        //noinspection deprecation
        PlaybackControlView controlView = binding.videoPlayer.findViewById(R.id.exo_controller);
        ImageView fullscreen = controlView.findViewById(R.id.exo_fullscreen_icon);
        text_not_available = controlView.findViewById(R.id.text_not_available);
        fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initFullscreenButton();
            }
        });
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerError(ExoPlaybackException error) {
                text_not_available.setText("VIDEO PLAYER ERROR");
                text_not_available.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    binding.progressBar.setVisibility(View.GONE);
                } else if (playbackState == Player.STATE_READY) {
                    binding.progressBar.setVisibility(View.GONE);
                } else if (playbackState == Player.STATE_BUFFERING) {
                    binding.progressBar.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_ENDED) {
                    binding.progressBar.setVisibility(View.GONE);
                } else {
                    binding.progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private MediaSource mediaSource(Uri uri) {
        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory().setMp4ExtractorFlags(Mp4Extractor.FLAG_WORKAROUND_IGNORE_EDIT_LISTS);
        extractorsFactory.setConstantBitrateSeekingEnabled(true);
        DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory(Util.getUserAgent(activity, activity.getPackageName()));
        return new ProgressiveMediaSource.Factory(dataSourceFactory, extractorsFactory).createMediaSource(uri);
    }


    private void initFullscreenButton() {
        if (mExoPlayerFullscreen) {
            showSystemUI();
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            binding.imageBigHolder.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, (int) Helpers.toDp(activity, 230)));
            binding.constraintBottom.setVisibility(View.VISIBLE);
            mExoPlayerFullscreen = false;
        } else {
            hideSystemUI();
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            binding.imageBigHolder.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
            binding.constraintBottom.findViewById(R.id.constraint_bottom).setVisibility(View.GONE);
            mExoPlayerFullscreen = true;

        }
    }

    private void hideSystemUI() {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

    }

    private void showSystemUI() {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    protected void isPause(){
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    protected void onBackPressed(){
        if(mExoPlayerFullscreen){
            showSystemUI();
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            binding.imageBigHolder.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, (int) Helpers.toDp(activity, 230)));
            binding.constraintBottom.setVisibility(View.VISIBLE);
            mExoPlayerFullscreen = false;
        } else {
            if (player != null) {
                player.stop();
                player.release();
                player = null;
                binding.videoPlayer.setPlayer(null);
                player = null;
                activity.finish();
            }
        }
    }
}
