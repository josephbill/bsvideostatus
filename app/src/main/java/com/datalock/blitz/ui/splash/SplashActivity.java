package com.datalock.blitz.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.ui.main.MainActivity;
import com.datalock.blitz.ui.registerlogin.LoginRegisterActivity;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                if(report.areAllPermissionsGranted()){
                    loadAbout();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }

    private void loadNative(){
        AdLoader.Builder builder = new AdLoader.Builder(this, Variable.native_id);
        AdLoader adLoader = builder.forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAds) {
                        Variable.unifiedNativeAd = unifiedNativeAds;
                        Variable.native_loaded = true;
                        runActivity();
                    }
                }).withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Variable.native_loaded = false;
                runActivity();
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), 1);
    }

    private void loadAbout(){
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "settings");
        new BSJson.Builder(this)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                Variable.app_email = objJson.getString("app_email");
                                Variable.app_logo = objJson.getString("app_logo");
                                Variable.banner_options = Boolean.parseBoolean(objJson.getString("banner_options"));
                                Variable.banner_id = objJson.getString("banner_id");
                                Variable.interstitial_options = Boolean.parseBoolean(objJson.getString("interstitial_options"));
                                Variable.interstitial_id = objJson.getString("interstital_id");
                                Variable.interstitial_click = Integer.parseInt(objJson.getString("interstital_click"));
                                Variable.native_options = Boolean.parseBoolean(objJson.getString("native_options"));
                                Variable.native_id = objJson.getString("native_id");
                                Variable.privacy_police = objJson.getString("privacy_police");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loadNative();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                    }
                })
                .load();
    }

    private void runActivity(){
        if(SharedPref.getSharedPref(this).contains(LoginPresenter.LOGIN_OR_SKIP)){
            if(!SharedPref.getSharedPref(this).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.NONE)){
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class));
                finish();
            }
        } else {
            startActivity(new Intent(SplashActivity.this, LoginRegisterActivity.class));
            finish();
        }
    }
}
