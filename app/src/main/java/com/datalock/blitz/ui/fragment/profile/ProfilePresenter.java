package com.datalock.blitz.ui.fragment.profile;

import android.app.Activity;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;

import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.base.RecyclerViewClickListener;
import com.datalock.blitz.data.model.ModelCategories;
import com.datalock.blitz.data.model.ModelHome;
import com.datalock.blitz.data.model.ModelUsers;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.Variable;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.adapter.HomeAdapter;

public class ProfilePresenter extends BasePresenter<ProfileView> {
    private Activity activity;
    private View rootView;
    private ImageView profileImage, button_edit_follow;
    private TextView profileName, profileBio, following, follower;
    private LinearLayout layout_no_data;
    private ProgressLoader progressLoader;
    protected void initView(Activity activity, View rootView){
        this.activity = activity;
        this.rootView = rootView;
        progressLoader = new ProgressLoader(activity);
        profileImage = rootView.findViewById(R.id.profileImage);
        profileName = rootView.findViewById(R.id.profileName);
        profileBio = rootView.findViewById(R.id.profileBio);
        following = rootView.findViewById(R.id.following);
        follower = rootView.findViewById(R.id.follower);
        layout_no_data = rootView.findViewById(R.id.layout_no_data);
        button_edit_follow = rootView.findViewById(R.id.button_edit_follow);
        loadUserDetail();
    }

    private void loadUserDetail(){
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "user_detail");
        if(SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID).equals(Variable.user_id)){
            jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
            button_edit_follow.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_profile_edit));
            button_edit_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMvpView().editProfile();
                }
            });
        } else {
            jsObj.addProperty("user_id", Variable.user_id);
            checkFollower();
        }
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            Glide.with(activity)
                                    .load(objJson.getString("user_image"))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .centerCrop()
                                    .placeholder(R.drawable.profile_default)
                                    .error(R.drawable.profile_default)
                                    .into(profileImage);
                            profileName.setText(objJson.getString("user_name"));
                            profileBio.setText(objJson.getString("user_bio"));
                            following.setText(objJson.getString("user_following"));
                            follower.setText(objJson.getString("user_follower"));
                            loadVideo();
                            progressLoader.stopLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void checkFollower() {
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "check_follower");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", Variable.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            JSONObject objJson = jsonArray.getJSONObject(0);
                            if (objJson.getInt("success") == 1) {
                                button_edit_follow.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_profile_unfollow));
                                button_edit_follow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendUnFollow();
                                    }
                                });
                            } else {
                                button_edit_follow.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_profile_follow));
                                button_edit_follow.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendFollow();
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {

                    }
                })
                .load();
    }
    private void sendUnFollow() {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "un_follow");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", Variable.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        button_edit_follow.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_profile_follow));
                        progressLoader.stopLoader();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void sendFollow() {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "follow");
        jsObj.addProperty("user_id", SharedPref.getSharedPref(activity).read(LoginPresenter.USER_ID));
        jsObj.addProperty("follow_id", Variable.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        button_edit_follow.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_profile_unfollow));
                        progressLoader.stopLoader();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void loadVideo(){
        Variable.arrayListProfile = new ArrayList<>();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "video_by_profile");
        jsObj.addProperty("vid", Variable.user_id);
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(responseBody));
                            JSONArray jsonArray = jsonObject.getJSONArray(Variable.TAG_ROOT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objJson = jsonArray.getJSONObject(i);
                                int vid = objJson.getInt("vid");
                                int cat_id = objJson.getInt("cat_id");
                                String video_title = objJson.getString("video_title");
                                String video_time = objJson.getString("video_time");
                                String video_image = objJson.getString("video_image");
                                String video_url = objJson.getString("video_url");
                                String video_type = objJson.getString("video_type");
                                String video_view = objJson.getString("video_view");
                                String video_share = objJson.getString("video_share");

                                int user_id = objJson.getInt("user_id");
                                String user_name = objJson.getString("user_name");
                                String user_image = objJson.getString("user_image");

                                String category_name = objJson.getString("category_name");

                                int total_comment = objJson.getInt("total_comment");
                                Variable.arrayListProfile.add(new ModelHome(vid, cat_id, video_title, video_time, video_image, video_url, video_type, video_view, video_share, total_comment,
                                        new ModelUsers(user_id, user_name, user_image), new ModelCategories(category_name)));
                            }
                            initData();
                            progressLoader.stopLoader();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }

    private void initData(){
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        if(Variable.arrayListProfile.size() == 0){
            layout_no_data.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        HomeAdapter homeAdapter = new HomeAdapter(activity, Variable.arrayListProfile, new RecyclerViewClickListener<ModelHome>() {
            @Override
            public void onItemClick(View view, ModelHome item, int i) {
                switch (i){
                    case 0:
                        getMvpView().startActivity(view, item.video_title);
                        break;
                    case 1:
                        break;
                }

            }
        });
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(homeAdapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(1000);
        scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator(0.5f));
        recyclerView.setAdapter(scaleInAnimationAdapter);
        progressLoader.stopLoader();
    }
}
