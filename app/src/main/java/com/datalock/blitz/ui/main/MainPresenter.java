package com.datalock.blitz.ui.main;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.benkkstudio.bsjson.API;
import com.benkkstudio.bsjson.BSJson;
import com.benkkstudio.bsjson.Interface.BSJsonOnSuccessListener;
import com.benkkstudio.bsmob.BSMob;
import com.benkkstudio.bsmob.Interface.BannerListener;
import com.datalock.blitz.Settings;
import com.datalock.blitz.ui.registerlogin.LoginRegisterActivity;
import com.datalock.blitz.ui.registerlogin.login.LoginPresenter;
import com.google.android.gms.ads.AdRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yarolegovich.slidingrootnav.SlideGravity;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;



import java.io.ByteArrayOutputStream;

import uc.benkkstudio.bsbottomnavigation.NavigationClickListner;
import uc.benkkstudio.bsvideostatus.R;

import com.datalock.blitz.data.base.BasePresenter;
import com.datalock.blitz.data.utils.CustomDialog;
import com.datalock.blitz.data.utils.ProgressLoader;
import com.datalock.blitz.data.utils.SharedPref;
import com.datalock.blitz.data.utils.ToasterDialog;
import com.datalock.blitz.data.utils.Variable;
import uc.benkkstudio.bsvideostatus.databinding.ActivityMainBinding;

import com.datalock.blitz.ui.search.SearchActivity;

public class MainPresenter extends BasePresenter<MainView> {
    private ActivityMainBinding binding;
    private Activity activity;
    public SlidingRootNav slidingRootNav;
    private ProgressLoader progressLoader;
    protected void initView(final Activity activity, final ActivityMainBinding binding) {
        this.activity = activity;
        this.binding = binding;
        progressLoader = new ProgressLoader(activity);

        initBottomNavigation();
        initDrawer();
        binding.imageWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMvpView().whatsappClick();
                binding.toolbarTitle.setText(activity.getString(R.string.app_name));
            }
        });
        binding.imageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SearchActivity.class);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, v, "robot");
                activity.startActivity(intent, options.toBundle());
            }
        });
    }



    private void startAnimation(View view){
        ObjectAnimator animY = ObjectAnimator.ofFloat(view, "translationY", -100f, 0f);
        animY.setDuration(1000);
        animY.setInterpolator(new BounceInterpolator());
        animY.start();
    }

    private void initBottomNavigation() {
        binding.bottomNavigation.addButton("HOME", R.drawable.ic_nav_home);
        binding.bottomNavigation.addButton("CATEGORY", R.drawable.ic_nav_category);
        binding.bottomNavigation.addButton("BOOKMARK", R.drawable.ic_bookmark_filled);
        binding.bottomNavigation.addButton("PROFILE", R.drawable.ic_menu_profile);
        binding.bottomNavigation.showIconOnly();
        binding.bottomNavigation.setSelectedItem(0);
        binding.bottomNavigation.setButtonClickListener(new NavigationClickListner() {
            @Override
            public void onCentreButtonClick() {
                if(!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)){
                    getMvpView().centreClick();
                } else {
                    ToasterDialog.showLogin(activity);
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        getMvpView().homeNavClick();
                        binding.toolbarTitle.setText(activity.getString(R.string.app_name));
                        break;
                    case 1:
                        getMvpView().categoriesNavClick();
                        binding.toolbarTitle.setText("CATEGORIES");
                        break;
                    case 2:
                        getMvpView().bookmarkNavClick();
                        binding.toolbarTitle.setText("BOOKMARKS");
                        break;
                    case 3:
                        if(!SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)){
                            getMvpView().profileNavClick();
                            binding.toolbarTitle.setText("");
                        } else {
                            ToasterDialog.showLogin(activity);
                        }
                        break;
                }
                resetMenuButton();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void initDrawer() {
        slidingRootNav = new SlidingRootNavBuilder(activity)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withGravity(SlideGravity.LEFT)
                .withMenuLayout(R.layout.drawer_layout)
                .inject();
        TextView headerEmail = slidingRootNav.getLayout().findViewById(R.id.headerEmail);
        headerEmail.setText(Variable.app_email);
        final LinearLayout ads_view = slidingRootNav.getLayout().findViewById(R.id.adsview);
        new BSMob.banner(activity)
                .setAdRequest(new AdRequest.Builder().build())
                .setId(Variable.banner_id)
                .setLayout(ads_view)
                .setListener(new BannerListener() {
                    @Override
                    public void onAdFailedToLoad(int error) {
                        ads_view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAdLoaded() {
                        ads_view.setVisibility(View.VISIBLE);
                    }
                })
                .setSize(BSMob.adaptiveSize(activity))
                .show();
        ImageView theme_mode = slidingRootNav.getLayout().findViewById(R.id.theme_mode);
        if (SharedPref.getSharedPref(activity).contains("THEME_MODE")) {
            if(!SharedPref.getSharedPref(activity).readBoolean("THEME_MODE")){
                theme_mode.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_light_mode));
            } else {
                theme_mode.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_dark_mode));
            }
        }
        theme_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAnimation(view);
                slidingRootNav.closeMenu(true);
                if (SharedPref.getSharedPref(activity).contains("THEME_MODE")) {
                    if(!SharedPref.getSharedPref(activity).readBoolean("THEME_MODE")){
                        SharedPref.getSharedPref(activity).write("THEME_MODE", true);
                    } else {
                        SharedPref.getSharedPref(activity).write("THEME_MODE", false);
                    }
                } else {
                    SharedPref.getSharedPref(activity).write("THEME_MODE", true);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.startActivity(new Intent(activity, MainActivity.class));
                        activity.finish();
                    }
                }, 500);
            }
        });
        slidingRootNav.getLayout().findViewById(R.id.menuHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                binding.bottomNavigation.setSelectedItem(0);
            }
        });

        slidingRootNav.getLayout().findViewById(R.id.menuCategories).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                binding.bottomNavigation.setSelectedItem(1);
            }
        });
        slidingRootNav.getLayout().findViewById(R.id.menuFavourite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                binding.bottomNavigation.setSelectedItem(2);
            }
        });
        slidingRootNav.getLayout().findViewById(R.id.menuProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                binding.bottomNavigation.setSelectedItem(3);
            }
        });

        slidingRootNav.getLayout().findViewById(R.id.menuShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Download " + activity.getString(R.string.app_name) + " for Whatsapp in : " + "\n" + "https://play.google.com/store/apps/details?id=" + activity.getPackageName());
                sendIntent.setType("text/plain");
                activity.startActivity(sendIntent);
            }
        });

        slidingRootNav.getLayout().findViewById(R.id.menuRate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + activity.getPackageName());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        });

        slidingRootNav.getLayout().findViewById(R.id.menuMore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.closeMenu(true);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=" + activity.getString(R.string.play_developer_id)));
                activity.startActivity(intent);
            }
        });

        slidingRootNav.getLayout().findViewById(R.id.menuPrivacy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialog customDialog = new CustomDialog(activity);
                customDialog.setTitle("Privacy Police");
                customDialog.setDialogType(CustomDialog.ONE_BUTTON);
                customDialog.setMessage("" + Html.fromHtml(Variable.privacy_police));
                customDialog.show();
                slidingRootNav.closeMenu(true);
            }
        });

        ImageView imageLogout = slidingRootNav.getLayout().findViewById(R.id.imageLogout);
        TextView menuLogout = slidingRootNav.getLayout().findViewById(R.id.menuLogout);
        if(SharedPref.getSharedPref(activity).read(LoginPresenter.LOGIN_OR_SKIP).equals(LoginPresenter.SKIP)){
            imageLogout.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_menu_login));
            menuLogout.setText("Login");
            menuLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivity(new Intent(activity, LoginRegisterActivity.class));
                }
            });
        } else {
            imageLogout.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_menu_logout));
            menuLogout.setText("Logout");
            menuLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPref.getSharedPref(activity).write(LoginPresenter.LOGIN_OR_SKIP, LoginPresenter.NONE);
                    SharedPref.getSharedPref(activity).write(LoginPresenter.USER_ID, "");
                    Intent intent = new Intent(activity, LoginRegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                    activity.finish();
                }
            });
        }
        binding.imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingRootNav.openMenu(true);
            }
        });
    }

    public void resetMenuButton(){
        binding.imageMenu.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_menu));
        binding.imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingRootNav.openMenu(true);
            }
        });
    }



    protected void uploadProfile(final Bitmap bitmap) {
        progressLoader.show();
        JsonObject jsObj = (JsonObject) new Gson().toJsonTree(new API());
        jsObj.addProperty("method_name", "upload_profile_image");
        jsObj.addProperty("user_id", Variable.user_upload_id);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        jsObj.addProperty("image", Base64.encodeToString(byteArray, Base64.DEFAULT));
        new BSJson.Builder(activity)
                .setServer(Settings.server_url)
                .setObject(jsObj)
                .setPurchaseCode(Settings.purchase_code)
                .setListener(new BSJsonOnSuccessListener() {
                    @Override
                    public void onSuccess(int statusCode, byte[] responseBody) {
                        getMvpView().onUpload();
                        progressLoader.stopLoader();
                    }

                    @Override
                    public void onFiled(int statusCode, byte[] responseBody, Throwable error) {
                        progressLoader.stopLoader();
                    }
                })
                .load();
    }
}
