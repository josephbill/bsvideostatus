package com.datalock.blitz.ui.status;

import uc.benkkstudio.bsvideostatus.R;
import com.datalock.blitz.data.base.BaseFragment;
import uc.benkkstudio.bsvideostatus.databinding.FragmentStatusBinding;

public class StatusFragment extends BaseFragment<StatusView, StatusPresenter> {
    @Override
    protected void onStarting() {

    }

    @Override
    protected void onDestroyed() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_status;
    }

    @Override
    protected StatusPresenter initPresenter() {
        return new StatusPresenter(requireContext(), (FragmentStatusBinding) getDataBinding(), getFragmentManager());
    }
}
