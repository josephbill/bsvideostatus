package com.datalock.blitz.ui.status.statusadapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.datalock.blitz.ui.status.tab.TabImages;
import com.datalock.blitz.ui.status.tab.TabVideos;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int tab_count;

    public PagerAdapter(FragmentManager fm, int tab_count) {
        super(fm);
        this.tab_count = tab_count;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new TabImages();
            case 1:
                return new TabVideos();
            default:
                //noinspection ConstantConditions
                return null;
        }
    }

    @Override
    public int getCount() {
        return tab_count;
    }
}