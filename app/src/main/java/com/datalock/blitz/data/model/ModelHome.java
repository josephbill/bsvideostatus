package com.datalock.blitz.data.model;

public class ModelHome {
    public int vid;
    public int cat_id;
    public String video_title;
    public String video_time;
    public String video_image;
    public String video_url;
    public String video_type;
    public String video_view;
    public String video_share;
    public int total_comment;

    public ModelUsers modelUsers;
    public ModelCategories modelCategories;

    public int view_type;

    public ModelHome(int vid, int cat_id, String video_title, String video_time, String video_image, String video_url, String video_type, String video_view, String video_share, int total_comment, ModelUsers modelUsers, ModelCategories modelCategories, int view_type) {
        this.vid = vid;
        this.cat_id = cat_id;
        this.video_title = video_title;
        this.video_time = video_time;
        this.video_image = video_image;
        this.video_url = video_url;
        this.video_type = video_type;
        this.video_view = video_view;
        this.video_share = video_share;
        this.total_comment = total_comment;
        this.modelUsers = modelUsers;
        this.modelCategories = modelCategories;
        this.view_type = view_type;
    }

    public ModelHome(int vid, int cat_id, String video_title, String video_time, String video_image, String video_url, String video_type, String video_view, String video_share, int total_comment, ModelUsers modelUsers, ModelCategories modelCategories) {
        this.vid = vid;
        this.cat_id = cat_id;
        this.video_title = video_title;
        this.video_time = video_time;
        this.video_image = video_image;
        this.video_url = video_url;
        this.video_type = video_type;
        this.video_view = video_view;
        this.video_share = video_share;
        this.total_comment = total_comment;
        this.modelUsers = modelUsers;
        this.modelCategories = modelCategories;
    }

    public ModelHome(int view_type) {
        this.view_type = view_type;
    }
}
