package com.datalock.blitz.data.utils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import uc.benkkstudio.bsvideostatus.R;


public class ProgressLoader extends AlertDialog {

    public ProgressLoader(@NonNull Context context) {
        super(context);
        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }


    @Override
    public void show() {
        super.show();
        setContentView(R.layout.progress_layout);
    }

    @Override
    public void cancel() {
        super.cancel();
    }

    public void stopLoader() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 500);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}
