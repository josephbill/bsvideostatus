package com.datalock.blitz.data.utils;

import android.os.Environment;

public class SdCardHelper {
    public static boolean isSdCardPresent() {

        String state = Environment.getExternalStorageState();
        switch (state) {
            case Environment.MEDIA_MOUNTED:
                return true;
            case Environment.MEDIA_BAD_REMOVAL:
                return false;
            case Environment.MEDIA_REMOVED:
                return false;
        }
        return true;
    }
}
