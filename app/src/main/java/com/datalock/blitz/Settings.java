package com.datalock.blitz;

import java.io.Serializable;

import uc.benkkstudio.bsvideostatus.BuildConfig;

public class Settings implements Serializable{
    public static final String server_url = BuildConfig.SERVER_URL + "api.php";
    public static final String purchase_code = BuildConfig.PURCHASE_CODE;
    public static final boolean enable_scroll_bouncing_effect = true; // 3 second
    public static final int video_upload_size = 15; //set file size
    public static final long video_upload_duration = 30; //set video duration in second
    public static final int video_loading_position = 10;
    public static final int category_loading_position = 10;
    public static final String admin_name = "♛Admin";
}
