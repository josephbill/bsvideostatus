package com.datalock.blitz.ui.detail;

import android.view.View;

import com.datalock.blitz.data.base.MvpView;

public interface DetailView extends MvpView {
    void startActivity(View view, String tag);
}
